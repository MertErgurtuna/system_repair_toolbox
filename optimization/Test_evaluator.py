import unittest
from . import evaluator
from constants import stl_constants


class TestEvaluator(unittest.TestCase):

    def test_evaluator_unit_steps(self):
        # evaluate(formula, signal_file_name, label_file_name, stn, return_type, past_results=[]):
        folder_name = "test_data/unit_test/"
        result = evaluator.evaluate("A 0 1 x0 > 1", folder_name + "sgt_0", folder_name + "sgt_0_label", [])

        self.assertListEqual([0, 0, 5, 0, 5, 0], result.get_result(stl_constants.OptimizationObjective('detailed')))

    def test_evaluator_pwa_signal(self):
        # evaluate(formula, signal_file_name, label_file_name, stn, return_type, past_results=[]):
        folder_name = "test_data/unit_test/"
        result = evaluator.evaluate("A 0 1 x0 > 1", folder_name + "sgt_1", folder_name + "sgt_1_label", [])

        self.assertListEqual([0, 0, 3, 3, -1, 0], result.get_result(stl_constants.OptimizationObjective('detailed')))

    def test_evaluator_pwa_signal2(self):
        # evaluate(formula, signal_file_name, label_file_name, stn, return_type, past_results=[]):
        folder_name = "test_data/unit_test/"
        result = evaluator.evaluate("P 0 2 x0 > 1", folder_name + "sgt_1", folder_name + "sgt_1_label", [])

        self.assertListEqual([2, 0, 3, 1, 1, 0], result.get_result(stl_constants.OptimizationObjective('detailed')))

    def test_evaluator_pwa_signal3(self):
        # evaluate(formula, signal_file_name, label_file_name, stn, return_type, past_results=[]):
        folder_name = "test_data/unit_test/"
        result = evaluator.evaluate("P 0 2 x0 > 1", folder_name + "sgt_2", folder_name + "sgt_2_label", [])

        self.assertListEqual([2, 0, 3, 1, 1, 0], result.get_result(stl_constants.OptimizationObjective('detailed')))


if __name__ == '__main__':
    unittest.main()
