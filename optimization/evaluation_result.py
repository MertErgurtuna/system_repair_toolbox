from constants import stl_constants


def get_best_result(results, obj):
    """results: a list of EvaluationResult - parameters. Return the best pair w.r.to obj"""
    if not results:
        return None
    if obj.category == stl_constants.CATEGORY_MINIMIZATION:
        r = min((rt for rt in results if obj.check_bound(rt[0])),
                key=lambda item: item[0].get_result(obj), default=None)
    else:
        r = max((rt for rt in results if obj.check_bound(rt[0])),
                key=lambda item: item[0].get_result(obj), default=None)
    return r


class EvaluationResult:

    def __init__(self):
        self.tp = 0
        self.fp = 0
        self.tn = 0
        self.fn = 0
        self.qv_pos = 0
        self.qv_neg = 0
        self.qv = 0

    def update(self, label, result, duration, qv=0):
        self.qv += abs(qv)*duration
        if result == 1:  # Positive
            if label == 1:  # True Positive
                self.tp += duration
                self.qv_pos += qv
            else:  # False Positive
                self.fp += duration
        else:  # Negative
            if label == 1:  # False Negative
                self.fn += duration
                self.qv_neg += qv
            else:  # True Negative
                self.tn += duration

    def update_with_result(self, r):
        self.tp += r.tp
        self.fp += r.fp
        self.tn += r.tn
        self.fn += r.fn
        self.qv_pos += r.qv_pos
        self.qv_neg += r.qv_neg
        self.qv += r.qv

    def get_mismatch_count(self):
        return self.fp + self.fn

    def get_detailed_result(self):
        return [self.fp, self.fn, self.tp, self.tn, self.qv_pos, self.qv_neg]
        #false_positive, false_negative, true_positive, true_negative

    def set_worst(self):
        self.qv_pos = stl_constants.MAX_EVAL
        self.fp = stl_constants.MAX_EVAL
        self.fn = stl_constants.MAX_EVAL
        self.qv = stl_constants.MAX_EVAL

    def get_result(self, obj):
        if obj.name == 'detailed':
            return self.get_detailed_result()
        if obj.name == 'mismatch': # mismatch
            return self.fp + self.fn
        if obj.name == 'ratio': # ratio
            return float(self.fp + self.fn)/(self.fp + self.fn + self.tp + self.tn)

        if obj.name == 'tp':
            return self.tp
        if obj.name == 'tn':
            return self.tn
        if obj.name == 'qv':
            return self.qv
        # The rest is for f score, precision, recall
        total_p = self.tp + self.fp

        precision = 0
        if total_p > 0:
            precision = float(self.tp) / total_p
        if obj.name == 'precision':
            return precision

        real_p = self.tp + self.fn
        recall = 0
        if real_p > 0:
            recall = float(self.tp) / real_p
        if obj.name == 'recall':
            return recall

        if obj.name == 'f_score':
            if precision == 0 or recall == 0:
                return 0
            return float((1 + obj.beta * obj.beta) * precision * recall) / (obj.beta * obj.beta * precision + recall)

    def is_better(self, r, obj):

        if not obj.check_bound(r):
            return False # r does not satisfy the objective criteria.

        if obj.category == stl_constants.CATEGORY_MINIMIZATION:
            if r.get_result(obj) < self.get_result(obj):
                return True
            else:
                return False
            # MAXIMIZATION
        if obj.category == stl_constants.CATEGORY_MAXIMIZATION:
            if r.get_result(obj) > self.get_result(obj):
                return True
            else:
                return False

    def __str__(self):
        return "TP: " + str(self.tp) + " FP: " + str(self.fp) + " TN: " + \
               str(self.tn) + " FN: " + str(self.fn)

    def get_detailed_result_str(self):
        beta_scores = [1, 0.9, 0.5, 0.4, 0.3, 0.2, 0.15, 0.1]
        string_rep = str(self) + '\n ' + \
                     "Precision: " + str(self.get_result(stl_constants.OptimizationObjective('precision'))) + \
                     " Recall: " + str(self.get_result(stl_constants.OptimizationObjective('recall'))) + "\n" + \
                     " Fscore ".join([str(beta) + " " + str(self.get_result(stl_constants.OptimizationObjective('f_score', beta)))
                                     for beta in beta_scores])
        return string_rep

