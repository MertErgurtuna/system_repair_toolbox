import trace_checker.STL as STL
from constants import stl_constants
from util import metrics_util
from cause_mining_algo import helper_funs
from . import evaluation_result


def evaluate(formula, signal_file_name, label_file_name, stn, past_results=[], skip_count=10):
    """
    Evaluate the given formula along the signal, compare the results with the labels from the input file, and compute
    the total number of false positives, false negatives, true positives and true negatives. If there are past_results,
    all formulas are concatenated with disjunction, the resulting formula's evaluation results are returned.

    Args:
        formula:
        signal_file_name:
        label_file_name:
        stn:
        past_results:
        skip_count:

    Returns:
        An instance of EvaluationResult.
    """
    # Construct syntax tree
    if stn and past_results == []:
        stn.clean()
    else:
        # create new formula by concatenating the formula with past formulas with disjunction.
        # TODO: why do we do it here, why not the corresponding caller?
        new_formula = formula
        for frml in past_results:
            new_formula = ' | ' + new_formula + ' ' + frml.formula
        stn = STL.SyntaxTreeNode()
        stn.initialize_node(new_formula.split(), 0)

    result = evaluation_result.EvaluationResult()

    last_evaluation_time = 0
    last_evaluation = 0
    last_label = 0

    last_metrics = []

    with open(signal_file_name, 'r') as sf, open(label_file_name, 'r') as lf:
        for metric_data, label_data in zip(sf, lf):
            metrics = metric_data.split()
            t = metrics[0]
            metrics = metrics[1:]  # the metric data
            _, label = label_data.split()
            t = float(t)
            metrics = [float(x) for x in metrics]
            current_label = float(label)
            while t > stn.nt: # The evaluation changes before t, at nt re-evaluate with previous data
                duration = stn.nt - last_evaluation_time # The duration for the difference between the label and the evaluation
                stn.compute_qv(STL.DataPoint(value=last_metrics, time=stn.nt))

                if last_evaluation_time >= skip_count:
                    result.update(last_label, last_evaluation, duration, stn.qv)
                last_evaluation_time = last_evaluation_time + duration
                last_evaluation = stn.get_qualitative_evaluation()

            if last_evaluation_time < skip_count < t:  # Enforce an evaluation at the skip point
                stn.compute_qv(STL.DataPoint(value=last_metrics, time=skip_count))
                last_evaluation_time = skip_count
                last_evaluation = stn.get_qualitative_evaluation()
            stn.compute_qv(STL.DataPoint(value=metrics,time=t))
            duration = t - last_evaluation_time
            if last_evaluation_time >= skip_count:
                result.update(last_label, last_evaluation, duration, stn.qv)
            last_evaluation_time = t
            last_evaluation = stn.get_qualitative_evaluation()

            last_metrics = metrics
            last_label = current_label

    return result


def evaluate_signals(formula, folder_name, signal_file_base, trace_count, signal_file_rest, stn=None,
                     past_results=[], skip_count=10):
    """
    Checks if the formula's lower bounds & upper bounds are within the limits. Calls evaluate, returns valuation based
    on return_type.
    Args:
        formula: prefix formula
        folder_name:
        signal_file_base:
        trace_count:
        signal_file_rest:
        stn:
        past_results:
        skip_count:

    Returns:
        Returns an instance of EvaluationResult.
    """

    # Below code piece checks if lower bounds are smaller than upper bounds in "formula",
    # if not, it returns the worst value.
    if not stn:
        stn = STL.SyntaxTreeNode()
        stn.initialize_node(formula.split(), 0)

    flag = helper_funs.traverse_formula_check_intervals(stn)
    result = evaluation_result.EvaluationResult()
    if not flag:
        result.set_worst()
        return result

    for i in range(trace_count):
        s_file = folder_name + signal_file_base + '_' + str(i) + signal_file_rest
        label_file = s_file + "_label"
        r = evaluate(formula=formula, signal_file_name=s_file, label_file_name=label_file, stn=stn,
                     past_results=past_results, skip_count=skip_count)
        result.update_with_result(r)

    return result


