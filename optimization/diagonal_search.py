from trace_checker import STL
from optimization import evaluator
from optimization import formula_search
from constants import stl_constants

def search_template_formulas(template_formula_list, folder_name, signal_file_base, trace_count, pc, parameter_domains,
                        bound, max_number_of_disjunctions, log_file_name=None,
                        verbose=False):
    prev_for = 'F'
    is_terminated = False
    diagonal_optimization = True;
    prev_tp = 0
    current_best =0;
    firstIteration = True
    for i in range(max_number_of_disjunctions+1):
        return_type = stl_constants.OptimizationObjective(name='tp', bound=bound*(i+1))
        formula_list = []
        not_opt_count = 0
        if prev_for != 'F':
            for index,f in enumerate(template_formula_list):
                formula_list.append(prev_for + " |  ( " + f + " ) " )
        else:
            for index,f in enumerate(template_formula_list):
                formula_list.append(" ( " + f + " ) ")
        results = {f: {} for f in formula_list}
        if firstIteration:
            first_run_results = {f: {} for f in formula_list}
        templates = {f: {} for f in template_formula_list}
        index = 0;
        for template_formula,template in zip(formula_list,templates):
            index = index +1
            formula, parameter_domains_for_formula = formula_search.generate_formula_from_template(template_formula, parameter_domains)
            if firstIteration:
                results[template_formula] = formula_search.parameter_search_for_formula(formula=formula,
                                                                         parameter_domains_for_formula=parameter_domains_for_formula,
                                                                         folder_name=folder_name,
                                                                         trace_count=trace_count,
                                                                         signal_file_base=signal_file_base,
                                                                         process_count=pc,
                                                                         return_type=return_type,
                                                                         past_results=[],
                                                                         diagonal_optimization=diagonal_optimization,
                                                                         log_file=False,
                                                                         skip_count=10,
                                                                         verbose=verbose)

            else:
                if prev_tp + first_run_results[" ( " + template + " ) "].valuation > current_best:
                    results[template_formula] = formula_search.parameter_search_for_formula(formula=formula,
                                                                         parameter_domains_for_formula=parameter_domains_for_formula,
                                                                         folder_name=folder_name,
                                                                         trace_count=trace_count,
                                                                         signal_file_base=signal_file_base,
                                                                         process_count=pc,
                                                                         return_type=return_type,
                                                                         past_results=[],
                                                                         diagonal_optimization=diagonal_optimization,
                                                                         log_file=False,
                                                                         skip_count=10,
                                                                         verbose=verbose)
                    if results[template_formula].valuation > current_best:
                        current_best = results[template_formula].valuation
                else:
                    not_opt_count = not_opt_count +1
                    results[template_formula] = None

        all_formulas = []
        if firstIteration:
            firstIteration = False
            first_run_results = results
        for result, template in zip(results.items(),templates.items()):
            if result[1] is not None:
                valuation = result[1]
                if valuation.valuation > 0:
                    all_formulas.append(valuation)
                    if valuation.valuation == prev_tp:
                        template_formula_list.remove(template[0])
                else:
                    template_formula_list.remove(template[0])

        # Sort in descending order according to the optimal criteria
        formulas_sorted = sorted(all_formulas, key=lambda x: x.valuation, reverse=True)
        prev_iteration_result = prev_for
        prev_for = formulas_sorted[0][0]
        v = evaluator.evaluate_signals(formula=STL.infix_to_prefix(prev_for),
                                       folder_name=folder_name,
                                       signal_file_base=signal_file_base,
                                       trace_count=trace_count,
                                       signal_file_rest='',
                                       stn=None,
                                       past_results=[])

        if v.fn == 0:
            print(prev_for, str(v))
            is_terminated = True
            break
        if prev_tp == v.tp:
            v = evaluator.evaluate_signals(formula=STL.infix_to_prefix(prev_iteration_result),
                                           folder_name=folder_name,
                                           signal_file_base=signal_file_base,
                                           trace_count=trace_count,
                                           signal_file_rest='',
                                           stn=None,
                                           past_results=[])
            print(prev_iteration_result, str(v))
            is_terminated = True
            break
        prev_tp = v.tp
        current_best = prev_tp
    if not is_terminated:
        print(prev_for, str(v))
