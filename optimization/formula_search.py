

from signal_generation import generator
from trace_checker import formula_generator
from trace_checker import STL
from optimization import grid_search
import json
from constants import stl_constants
from util import diagonal_search_util
import itertools



def save_formula_search_results(folder_name, results, best_formulas, result_file):
  """

  Args:
    folder_name:
  """
  file_name = folder_name + 'formula_search_results' + result_file
  r = dict()
  r['results'] = results
  r['best_formulas'] = best_formulas

  j = json.dumps(r)
  with open(file_name+".txt", 'w') as outfile:
    json.dump(j, outfile)


def formula_search(metric_list, set_valued_metrics, operator_counts, parameter_domains, folder_name, trace_count,
                   signal_file_base, process_count, save, return_type, result_file,
                   cause_mining=False, control_metrics=[], past_results=[], withoutS=False, time_shift=0,
                   diagonal_optimization=False, log_file_name=None, skip_count=10, verbose=False):
  """
  calls formula_search_operator_count to find a best formula for all given oc's in operator_counts list, returns all of
  the best formulas and the best of them

  Args:
    metric_list: all metrics that will be used in generated formulas
    set_valued_metrics: metrics that take discrete values within a given set
    operator_counts: (list of integers with ascending order !!) how many operators will be within metrics in the
    generated formulas. formula_search will find a best_formula (based on the given valuation method) f
    or all operator_counts given in this list
    parameter_domains:
    folder_name:
    trace_count:
    signal_file_base:
    process_count: for parallel processing
    save:
    return_type: determines the valuation that will be used to choose the best formulas. One of the types
    from stl_constants.
    result_file:
    cause_mining: (bool) formulas will be generated with generate_formula_tree_A_added if cause_mining==True, otherwise,
    they will be generated with generate_formula_tree
    control_metrics: the metrics that we have the control of, i.e. can change in the controller (for traffic system case
    control metrics are traffic lights whereas other metrics are road business rates)
    past_results: past best formulas. New formulas will be chosen according to their valuation "together with" the past
    best formulas.
    withoutS: (bool) if True, the formulas are generated without Since
    time_shift:
    diagonal_optimization: (bool) if True, diagonal optimization will be used for formula search
    log_file_name:
    skip_count: The initial time duration for which the evaluation will not effect the result.
  Returns: best_formulas (best formulas of all operator counts in operator_counts list), formula (the formula with
  the biggest oc i.e. necessarily the formula with the best valuation)
    verbose: Set to True for detailed print to log file.
"""

  log_file = None
  if log_file_name != None:
    log_file = open(folder_name + log_file_name, 'w', buffering=1)
  # Perform the search for all given formula counts.
  results = {oc: [] for oc in operator_counts}
  best_formulas = {oc: [] for oc in operator_counts}
  for oc in operator_counts:
    results[oc], best_formulas[oc] = formula_search_operator_count(metric_list=metric_list,
                                                                   control_metrics=control_metrics,
                                                                   set_valued_metrics=set_valued_metrics, oc=oc,
                                                                   parameter_domains=parameter_domains,
                                                                   folder_name=folder_name,
                                                                   trace_count=trace_count,
                                                                   signal_file_base=signal_file_base,
                                                                   cause_mining=cause_mining,
                                                                   process_count=process_count,
                                                                   return_type=return_type,
                                                                   past_results=past_results, withoutS=withoutS,
                                                                   time_shift=time_shift,
                                                                   diagonal_optimization=diagonal_optimization,
                                                                   log_file=log_file,
                                                                   skip_count=skip_count,
                                                                   verbose=verbose)


    #print "Results for oc %d " % oc
    #if cause_mining:
    #  print best_formulas[oc]
    if best_formulas[oc].valuation == stl_constants.MIN_EVAL and return_type.category == stl_constants.CATEGORY_MINIMIZATION:
      break

  if log_file:
    log_file.write("----------- END OF FORMULA SEARCH, PRINT RESULTS FOR EACH OPERATOR COUNT -------------\n")
    for oc in operator_counts:
        log_file.write("BEST FOR OC = " + str(oc) + " : " + best_formulas[oc].formula +
                       " valuation = " + str(best_formulas[oc].valuation) + "\n")
  if save:
    save_formula_search_results(folder_name, results, best_formulas, result_file)

  if log_file:
    log_file.close()

  formula = best_formulas[oc]
  if diagonal_optimization:
    return results, None, None
  if return_type.category == stl_constants.CATEGORY_MINIMIZATION:
    for oc in operator_counts:
      if best_formulas[oc] and formula.valuation > best_formulas[oc].valuation:
        formula = best_formulas[oc]
  elif return_type.category == stl_constants.CATEGORY_MAXIMIZATION:
    for oc in operator_counts:
      if best_formulas[oc] and formula.valuation < best_formulas[oc].valuation:
        formula = best_formulas[oc]
  return best_formulas, formula, results


def formula_search_operator_count(metric_list, set_valued_metrics, oc, parameter_domains, folder_name, trace_count,
                                  signal_file_base, process_count, return_type, cause_mining=False, control_metrics=[],
                                  past_results=[], withoutS=False, time_shift=0, diagonal_optimization=False,
                                  log_file=None, skip_count=10, verbose=False):
  """
   For the given operator count, compute the number of template formulas and
      run grid search for each template formula.

   Args:
      metric_list: list of metrics that will appear in formulas
      set_valued_metrics: metrics that take discrete values from a predefined set
      oc: (integer) how many operators will be in the resulting formula between metrics
      parameter_domains:
      folder_name:
      trace_count:
      signal_file_base:
      return_type: determines the valuation that will be used to choose the best formulas. One of the types
      from stl_constants
      cause_mining: (boolean) if we are using this function for cause mining, the generated formulas will be A added
      process_count: for parallel processing
      control_metrics: the metrics that we can control/change. These metrics' values will be changed and the systems
      will be resimulated to reduce the number of bad labeled data points in the future. In the traffic system case,
      control metrics are traffic lights and the other metrics are road business rates.
      past_results: past best formulas. New formulas will be chosen according to their valuation "together with" the
      past best formulas.
      withoutS: (bool) if True, the formulas are generated without Since
      time_shift:
      diagonal_optimization: (bool) if True, diagonal optimization will be used for formula search
      skip_count: The initial time duration for which the evaluation will not effect the result.
  Returns: results (all formulas generated with given oc and their valuations), best formula (and its valuation)
  """
  if cause_mining:
    template_formula_list = formula_generator.generate_formula_tree_for_cause_mining(metric_list=metric_list,
                                                                                     control_metrics=control_metrics,
                                                                                     operator_count=oc,
                                                                                     set_valued_metrics=set_valued_metrics,
                                                                                     parameter_domains=parameter_domains,
                                                                                     withoutS=withoutS)

  else:
    template_formula_list = formula_generator.generate_formula_tree_iterative(metric_list=metric_list,
                                                                              operator_count=oc,
                                                                              return_formula_string=True,
                                                                              set_valued_metrics=set_valued_metrics,
                                                                              withoutS=withoutS)
    if time_shift > 0:
        time_shift_str = str(time_shift) + " " + str(time_shift)
        template_formula_list = ['P ' + time_shift_str + " ( " + f + " )" for f in template_formula_list]
    if diagonal_optimization:
        template_formula_list = [formula for formula in template_formula_list if formula.split().count('S') < 1]
        template_formula_list = [formula for formula in template_formula_list if formula.split().count('|') < 1]

  #print "----------- For operator count " + str(oc) + ", " + str(len(template_formula_list)) + " formulas will be tested."
  # Process and remove the formulas with double Since.
  #template_formula_list = [formula for formula in template_formula_list if formula.split().count('S') == 0]
  if log_file:
    log_file.write("----------- For operator count " + str(oc) + ", " + str(len(template_formula_list)) + " formulas will be tested.\n")


  # For each template formula, run a grid search. Store the results, remember the best one.
  results = {f:{} for f in template_formula_list}
  best_formula = stl_constants.FormulaValuation(formula='False', valuation=stl_constants.MIN_EVAL)
  if return_type.category == stl_constants.CATEGORY_MINIMIZATION:
    best_formula = stl_constants.FormulaValuation(formula='False', valuation=stl_constants.MAX_EVAL)

  for template_formula in template_formula_list:
    formula, parameter_domains_for_formula = generate_formula_from_template(template_formula, parameter_domains)
    if log_file:
      log_file.write("Optimization for formula: " + formula + "\n")
    results[template_formula] = parameter_search_for_formula(formula=formula,
                                                             parameter_domains_for_formula=parameter_domains_for_formula,
                                                             folder_name=folder_name,
                                                             trace_count=trace_count,
                                                             signal_file_base=signal_file_base,
                                                             process_count=process_count, return_type=return_type,
                                                             past_results=past_results,
                                                             diagonal_optimization=diagonal_optimization,
                                                             log_file=log_file,
                                                             skip_count=skip_count,
                                                             verbose=verbose)
    if diagonal_optimization:
        continue
    if (return_type.category == stl_constants.CATEGORY_MAXIMIZATION and
            results[template_formula].valuation > best_formula.valuation) or (
        return_type.category == stl_constants.CATEGORY_MINIMIZATION and
            results[template_formula].valuation < best_formula.valuation):
      #  UPDATE BEST:
      best_formula = results[template_formula]
      if log_file:
        log_file.write("(***   UPDATE BEST: " + best_formula.formula + " " + str(best_formula.valuation) + '\n')
      if return_type.category == stl_constants.CATEGORY_MINIMIZATION and best_formula.valuation == stl_constants.MIN_EVAL:
        break # No need to check the rest
  return results, best_formula


def parameter_search_for_formula(formula, parameter_domains_for_formula, folder_name, trace_count, signal_file_base,
                                 process_count, return_type, past_results=[], diagonal_optimization=False,
                                 log_file=None, skip_count=10, verbose=False):
  parameter_list = list(parameter_domains_for_formula.keys())
  parameter_domain = [parameter_domains_for_formula[pa] for pa in parameter_list]

  prefix_formula = STL.infix_to_prefix(formula)

  if diagonal_optimization:
    best_v, params, time_passed = diagonal_search_util.get_optimization_result(parameter_list,parameter_domain,
                                                                               prefix_formula,folder_name,signal_file_base,
                                                                               trace_count, return_type, process_count,
                                                                               log_file=log_file,
                                                                               verbose=verbose)
  else:
    best_v, params, time_passed = grid_search.grid_search(formula=prefix_formula, parameter_list=parameter_list,
                                                          parameter_domain=parameter_domain, folder_name=folder_name,
                                                          signal_file_base=signal_file_base, trace_count=trace_count,
                                                          signal_file_rest='', process_count=process_count,
                                                          return_type=return_type, past_results=past_results,
                                                          skip_count=skip_count)
  formula_n = formula
  if params == None:
    if log_file:
      log_file.write( "There are no best parameters for the formula: " + formula + "\n")
  else:
    for p, v in zip(parameter_list, params):
      formula_n = formula_n.replace(p, str(v))
    if log_file:
      log_file.write("Valuation: " + str(best_v) + " formula " + formula_n + " time " + str(time_passed))
      '''
      if(best_v<=0):
        print( "There are no best parameters for the formula: " + formula + "\n")
      else:
        print("Valuation: " + str(best_v) + " formula " + formula_n + " time " + str(time_passed))
      '''

  return stl_constants.FormulaValuation(formula=formula_n, valuation=best_v)


def generate_formula_from_template(template_formula, parameter_domains):

  # first process the formula:
  formula_tokens = template_formula.split()
  # tokens that start with p and has 2 characters:
  parameters = [p for p in formula_tokens if p[0] == 'p']
  parameters_set = list(set(parameters))  # make unique

  parameter_domains_in_formula = {}
  for p in parameters_set:
    last_index_found = 0
    for i in range(parameters.count(p)):
      p_new = p + str(i)
      for j in range(last_index_found, len(formula_tokens)):
        if formula_tokens[j] == p:
          last_index_found = j
          # If formula_tokens[j][1] is 'T', then it is P pT pT, we want both to take the same value, like P 1 1, P 2 2, ... so we want both to become pT0 pT0. Since there is only 2 pT's in each formula, we can initialize them by hand
          if formula_tokens[j][1] == 'T':
            formula_tokens[j] = 'pT0'
            parameter_domains_in_formula['pT0'] = parameter_domains[p]
            break
          if formula_tokens[j][len(formula_tokens[j])-1] == 'f': # again both will take the same value:
            parameter_domains_in_formula[p] = parameter_domains[p[0:p.find('_')]]
            break
          formula_tokens[j] = p_new  # Burda find kullancaktim ama find da lineer zamanda is yapiyormus ve bulamayinca ValueError veriyor
          parameter_domains_in_formula[p_new] = parameter_domains[p]  # Bu da daha efektif yapilabilir.
          break
          # new_formula = new_formula.replace(p, p_new, 1)

  new_formula = " ".join(formula_tokens)
  #print(new_formula)
  return new_formula, parameter_domains_in_formula

