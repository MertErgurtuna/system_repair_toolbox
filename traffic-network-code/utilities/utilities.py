"""Methods used by traffic network classes."""
import collections
import numpy as np
import functools

LinkIntersection = collections.namedtuple('LinkIntersection', ['v_up', 'v_down'])


def str_point(pt):
  """Returns string representation of a point."""
  return "(%d,%d)" % (pt.x, pt.y)

def get_number_of_combinations(counts):
  if not counts:
    return 0
  return functools.reduce(lambda x, y: x * y, counts)

def index_to_mode(mode_counts, index):
  # 2,3,2   2 6 12
  # 0: 0 0 0
  # 1: 1 0 0
  # 2: 0 1 0
  # 3: 1 1 0
  # 4: 0 2 0
  # 5: 1 2 0
  # 6: 0 0 1
  # 7: 1 0 1
  # 8: 0 1 1
  # 9: 1 1 1
  if not mode_counts:
    return []
  c = len(mode_counts)
  indexes = np.zeros(c, int)
  counts = [functools.reduce(lambda x,y:x*y, mode_counts[0:l+1]) for l in range(c-1)]
  ci = c-1
  while ci > 0:
    c_tmp = counts[ci-1]
    indexes[ci] = index / c_tmp
    index = index % c_tmp
    ci -= 1
  indexes[0] = index
  return indexes

def mode_to_index(mode_counts, mode):
  if not mode_counts:
    return 0
  c = len(mode_counts)
  counts = [1] + [functools.reduce(lambda x,y:x*y, mode_counts[0:l+1]) for l in range(c-1)]
  return sum(i[0] * i[1] for i in zip(mode, counts))

def test_mode_conversion(mode_counts):
  c = len(mode_counts)
  counts = [functools.reduce(lambda x,y:x*y, mode_counts[0:l+1]) for l in range(c)]
  f = counts[-1]
  for i in range(f):
    if i != mode_to_index(mode_counts, index_to_mode(mode_counts, i)):
      print("ERROR " + str(i))
    else:
      print("SUCCESS " + str(i))

def generate_boundary_list(lower_bound, upper_bound, p_size, initial_array = []):
  boundary_count = int(1 + (upper_bound-lower_bound) / p_size)
  result = np.array(initial_array)
  result = np.sort(np.unique(
    np.append(result, [lower_bound, upper_bound] + [lower_bound + p_size * k for k in range(boundary_count)])
  ))
  if len(result) == 1:
    result = np.append(result, result)
  return result

def dict_to_string(d):
  return ", ".join([str(key) + ":" + str(value) for key, value in d.items()])


def set_to_bitmap(l):
  r = 0
  for i in l:
    r += (1 << i)
  return r

def bitmap_to_set(r, n):
  l = set()
  s = 1 << n
  c = n
  while s > 0 and r > 0:
    if r >= s:
      l.add(c)
      r -= s
    c -=1
    s >>= 1
  return l

