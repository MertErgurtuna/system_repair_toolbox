import collections

__all__ = ['utilities']
import utilities
LinkIntersection = collections.namedtuple('LinkIntersection', ['v_up', 'v_down'])
