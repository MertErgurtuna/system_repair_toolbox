"""
This module includes functions used for reading the input and constructing the data structures.
"""


class LoadError(Exception):
  pass

def _id_check(ids, name):
  if list(range(len(ids))) != sorted(ids):
    raise LoadError("The numbers of the % should be consecutive and start from 0. Given numbers: %s" % (
      name, ",".join(map(str, ids))))

def _parse_turn_ratios(turn_ratios):
  """Input is of the form link_no=tr,link_no=tr. Return a list of the form[(link_no, tr)].
  Args:
    turn_ratios: A string of the form "link_no=turn_ratio,link_no=turn_ratio"
  Returns:
    A list of pairs of the form [(link_no, turn_ratio)]
  """
  if turn_ratios:
    return [(int(wtmp[0]), float(wtmp[1])) for wtmp in [ws.split("=") for ws in turn_ratios.split(",")]]
  else:
    return []

def _parse_active_links(act_links):
  """ Input is a string specifying the possible active link sets.
  Args:
    act_links: A string of the form "label1=link_no|label2=link_no1,link_no2|label_3=."
     e.g. "a=0|b=1,2|c=."
  Returns:
    A dictionary of active links of the from {label:[link list]}
      e.g. {a:[0], b:[1,2], c:[]}
  """
  act_sets = {item[0]:item[1] for item in [tmp.split("=") for tmp in act_links.split("|")]}
  for key,value in act_sets.items():
    if value == ".":
      act_sets[key] = []
    else:
      act_sets[key] = [int(v) for v in value.split(",")]
  return act_sets

def _parse_supply_ratios(supply_ratios):
  """

  Args:
    supply_ratios: A string of the form
      "light_configuration-link_no=supply_ratio,link_no=supply_ratio|light_configuration-link_no=supply_ratio..."
      e.g. a-0=1|b-1=0.5,2=0.5
  Returns:
     A dictionary of parsed ratios of the form {light_configuration:[(link_no, supply_ratio)]}
    e.g. {a:[(0,1)], b:[(1,0.5), (2,0.5)]}
  """
  light_ratios_list = {item[0]:item[1] for item in [tmp.split("-") for tmp in supply_ratios.split("|")]}
  for key,value in light_ratios_list.items():
    light_ratios_list[key] = _parse_turn_ratios(value)
  return light_ratios_list


def load_from_annotated_file(file_name):
  """Read the input data from file, and construct and descriptors of links and intersections."""
  f = open(file_name, 'r')
  links = []
  intersections = []
  ids = []
  vids = []
  l_vids = []
  for line in f:
    l = line.rstrip().split(" ")
    if l[0] == "LINK":
      d_desc = {ws[0]:ws[1] for ws in [w.split(":") for w in l[1:]]}
      if "number" not in d_desc:
        raise LoadError("Link should have a number " + line)
        return
      d_desc["number"] = int(d_desc["number"])
      if d_desc["number"] in ids:
        raise LoadError("Link numbers should be unique %d" %  d_desc["number"])
        return
      ids.append(d_desc["number"])
      if "vup" not in d_desc and "vdown" not in d_desc:
        raise LoadError("A link should be connected to at least one intersection.")
        return
      if "vup" in d_desc:
        d_desc["vup"] = int(d_desc["vup"])
        l_vids.append(d_desc["vup"])
        # If there is an upstream intersection, the supply ratios should be specified
        if "supply_ratios" not in d_desc:
          raise LoadError("If there is an upstream intersection, the supply ratios should be specified. Link id %d ." %
                          d_desc["number"])
        d_desc["supply_ratios"] =  _parse_supply_ratios(d_desc["supply_ratios"])

      if "vdown" in d_desc:
        d_desc["vdown"] = int(d_desc["vdown"])
        l_vids.append(d_desc["vdown"])
        if "turn_ratios" not in d_desc and "output_link" not in d_desc:
          raise LoadError("If there is a downstream intersection, and the link is not an output link "
                          "the turn ratios should be specified. Link id %d ." %
                          d_desc["number"])
        d_desc["turn_ratios"] = _parse_turn_ratios(d_desc.get("turn_ratios", ""))

      if "output_link" in d_desc:
        d_desc["output_link"] = True

      if "linkdir" not in d_desc:
        raise LoadError("The direction of the link should be specified.")
        return
      if d_desc["linkdir"] not in ["v", "h"]:
        raise LoadError("Link direction can either be h or v.")
        return
      if "flowdir" not in d_desc:
        raise LoadError("The flow direction should be specified.")
        return
      if d_desc["flowdir"] not in ["+", "-"]:
        raise LoadError("The flow direction can either be + or -, got: %s for link %d" % (
          d_desc["flowdir"], d_desc["number"]))
        return
      links.append(d_desc)
      d_desc["dl_min"] = float(d_desc.get("dl_min", 0))
      d_desc["dl_max"] = float(d_desc.get("dl_max", 0))
      d_desc["dl_max_a"] = float(d_desc.get("dl_max_a", 0))
      d_desc["dl_max_b"] = float(d_desc.get("dl_max_b", 0))
    elif l[0] == "INTERSECTION":
      d_desc = {ws[0]: ws[1] for ws in [w.split(":") for w in l[1:]]}
      if "number" not in d_desc:
        raise LoadError("Intersection should have a number " + line)
        return
      d_desc["number"] = int(d_desc["number"])
      if d_desc["number"] in vids:
        raise LoadError("Intersection numbers should be unique %d" % d_desc["number"])
        return
      vids.append(d_desc["number"])
      if "act_set" not in d_desc:
        raise LoadError('Actuation set should be given for each intersection. ...'
                        'The actuation set can be in the following form: act_set:label=l1,label2=l2-l3-. meaning that ' \
                        'either link l1 or links l2 and l3 or none (.) can be actuated simultaneously')
        return
      # Parse the set of links that can be actuated at the same time.
      d_desc["act_set"] = _parse_active_links(d_desc["act_set"])

      intersections.append(d_desc)
    else:
      LoadError("Unknown line description: " + line)
  # Check if all the intersections refered by the links are defined with INTERSECTION:
  if len([tmp_id for tmp_id in l_vids if tmp_id not in vids]):
    raise LoadError("All the intersections referred by the links has to be defined with INTERSECTION keyword: %s-%s" %
                    (" ".join(map(str, l_vids)), " ".join(map(str, vids))))
  # Reading is done.
  # Check if the data is consistent.

  _id_check(ids, 'links')
  _id_check(vids, 'intersections')
  #tn = traffic-network-code.traffic_network.network.Network(links, intersections, 5)
  # return links and intersections descriptors
  return links, intersections


def load_propositions_from_file(file_name):
  """Read linear propositions from file and construct a list of tuples describing them.
  Args:
    file_name: A string. File contains propositions of the form: pi: link_id boundary less/greater.
      ith line should start with pi.
  Returns:
    A list of tuples of the form [(link_id boundary less_or_greater)]  1 for less (x_i < boundary), 0 for greater.
  """
  f = open(file_name, 'r')
  p_count = 0
  link_propositions = []
  signal_propositions = []
  for line in f:
    words = line.split(" ")
    if words[0][0] == "p": # + str(p_count) + ":":
      link_propositions.append(
        (int(words[0][1:-1]), int(words[1][1:]), float(words[3]), True if words[2] == "<" else False))
    elif words[0][0] == "s":
      signal_propositions.append((int(words[0][1:-1]), [int(l) for l in words[1].split(",")]))
    else:
      raise LoadError("Lines should start with p or s.")
    p_count += 1
  return link_propositions, signal_propositions

def parameter_set(parameter_list, parameter_value_list, formula_file, result_file):
  """ Read the formula file and replace each occurrence of  parameter_list[i] with parameter_value_list[i]
    for every i. Write the results to result_file"""
  f = open(formula_file, 'r')
  filedata = f.read()
  f.close()

  for i,p in enumerate(parameter_list):
    filedata = filedata.replace(parameter_list[i], parameter_value_list[i])

  f2 = open(result_file, 'w')
  f2.write(filedata)
  f2.close()
  return result_file

def parse_input_parameters(file_name):
  """Read input parameters from file.
  Args:
    file_name: A string. File contains input parameters (file names for the system, propositions and control):
  Returns:
    A list of file names.
  """
  f = open(file_name, 'r')
  system_file = ''
  proposition_file = ''
  control_file = ''
  overall_spec_file = ''
  step_count = 0
  link_count = 0
  signal_count = 0
  for line in f:
    words = line.rstrip().split(" = ")
    if words[0] == "system_file": # + str(p_count) + ":":
      system_file = words[1]
    elif words[0] == "proposition_file":
      proposition_file = words[1]
    elif words[0] == "control_file":
      control_file = words[1]
    elif words[0] == "overall_spec_file":
      overall_spec_file = words[1]
    elif words[0] == "step_count":
      step_count = int(words[1])
    elif words[0] == "link_count":
      link_count = int(words[1])
    elif words[0] == "signal_count":
      signal_count = int(words[1])
    else:
      raise LoadError("Lines should start with p or s.")
  return system_file, proposition_file, control_file, overall_spec_file, step_count, link_count, signal_count
