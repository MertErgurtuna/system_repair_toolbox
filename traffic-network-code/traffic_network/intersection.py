import numpy as np

#from external import graphics as g
from utilities import utilities
import random


class IntersectionError(Exception):
  pass

class Intersection(object):
  """The class representing an intersection in a traffic network model."""

  def __init__(self, desc, upstream_links, downstream_links, og_):
    self.number = desc["number"]
    self.upstream_links = upstream_links
    self.downstream_links = downstream_links
    self._act_sets = desc["act_set"]
    self.upstream_link_ids = [tmp["number"] for tmp in self.upstream_links]
    for k,s in self._act_sets.items():
      if len([a for a in s if a not in self.upstream_link_ids]):
        raise IntersectionError("Active links should be upstream of the intersection. %s" % ".".join(map(str, s)))
    self.down_stream_link_ids = [tmp["number"] for tmp in self.downstream_links]

    self._active_sets_keys = sorted(self._act_sets.keys()) # this will be used when an index is supplied

    # the turn ratios with respect to the mode will be kept here.
    self.local_alpha_beta_rate = {}


    self.og = og_
    self.supply_ratios = desc.get("supply_ratios", [])
    self.turn_ratios = desc.get("turn_ratios", [])
    #self.shape = g.Circle(g.Point(0,0), self.og.get_radius())
    #self.lights = [g.Circle(g.Point(0,0), self.og.get_light_width()) for _ in range(4)]
    #self.text = g.Text(g.Point(0, 0), "s" + str(self.number))

  def __str__(self):
    return "ID: %d, upstream_links: %s, downstream_links %s. Location: %s" % (
        self.number, " ".join(map(str, self.upstream_links)), " ".join(map(str, self.downstream_links)), utilities.str_point(self.shape.p1))

  def update_pos(self,x,y):
    """Updates the position if it is not already updated. Recursively update all the neighbours."""
    center = self.get_center()
    if not (center[0] == center[1] and center[0] == 0):
      return [] # already drawn
    self.shape.move(x,y)
    p = self.shape.getCenter()
    self.text.move(p.x, p.y)
    return_set = []
    for l in self.downstream_links:
      o = self.og.get_intersection_offsets(False, l['link_dir'], l['flow_dir'])
      return_set.append(['d',l['number'], x + o[0], y + o[1]])
    for l,light in zip(self.upstream_links, self.lights):
      o = self.og.get_intersection_offsets(True, l['link_dir'], l['flow_dir'])
      return_set.append(['u',l['number'], x + o[0], y + o[1]])
      p = self.og.get_light_offset(l['link_dir'], l['flow_dir'])
      light.move(p[0]+x, p[1]+y)
      if l['link_dir'] == 'v':
        light.setFill("red")
      else:
        light.setFill("green")


    return return_set

  def get_center(self):
    """Return the center of the circle."""
    return (self.shape.p1.x + self.shape.p2.x)/2, (self.shape.p1.y + self.shape.p2.y)/2

  def get_active_links(self, m):
    """Gets the mode index m/or mode m, returns the set of active links."""
    if isinstance(m, int):
      m = self._active_sets_keys[m]
    if m not in self._act_sets:
      raise Intersection("Intersection %s does not have a mode with label %s." % (self.number, m))
    return self._act_sets[m]

  def get_mode_count(self):
    return len(self._act_sets)

  def get_random_mode(self):
    return self._active_sets_keys[random.randrange(len(self._active_sets_keys))]

  def update(self, mode):
    if isinstance(mode, int):
      mode = self._active_sets_keys[mode]
    active_links = self._act_sets[mode]
    for link, light in zip(self.upstream_links, self.lights):
      if link["number"] in active_links:
        light.setFill("green")
      else:
        light.setFill("red")

  def draw(self, win):
    self.shape.draw(win)
    self.text.draw(win)
    for light in self.lights:
      light.draw(win)

  def get_mode_code(self, m):
    return self._active_sets_keys[m]

  def get_number_of_modes(self):
    return len(self._active_sets_keys)

  def get_downstream_links(self):
    return [l["number"] for l in self.downstream_links]

  def get_modes_for_link(self, l):
    ret = {key:[] for key in self._act_sets}
    for key in self._act_sets:
      if l in self._act_sets[key]:
        ret[key] = True
    return ret

  def get_mode(self, index):
    return self._active_sets_keys[index]

  def _get_link(self, number, isup):
    if isup:
      for li, l in enumerate(self.upstream_links):
        if l.number == number:
          return li, l
      return []
    else:
      for li, l in enumerate(self.downstream_links):
        if l.number == number:
          return li, l
      return []

  def is_active(self, intersection_mode, l):
    self_mode = intersection_mode
    if isinstance(intersection_mode, list):
      self_mode = intersection_mode[self.number]
    if not self_mode in self._act_sets: # DEBUG CODE TODO DELETE
      print(l, intersection_mode, self._act_sets.keys())
    if l in self._act_sets[self_mode]:
      return 1
    return 0

  def get_local_alpha_beta_rates(self, intersection_mode, l):
    """Return the rate vector for the given link id (global)."""
    self_mode = intersection_mode
    if isinstance(intersection_mode, list):
      self_mode = intersection_mode[self.number]
    local_index = self.upstream_link_ids.index(l)
    return self.local_alpha_beta_rate[self_mode][local_index]

  def set_local_alpha_beta_rates(self, all_links):
    """Set local rates alpha/beta for each intersection mode."""
    for key in self._act_sets:
      if len(self.down_stream_link_ids) == 0:
        self.local_alpha_beta_rate[key] = np.ones(len(self.upstream_link_ids))*np.inf
        # TODO WHAT IF THERE IS NO DOWNSTREAM LINK CASE IS HERE
      else:
        self.local_alpha_beta_rate[key] = np.ones((len(self.upstream_link_ids), len(self.down_stream_link_ids)))*np.inf
        for global_upstream_link_id in self._act_sets[key]:
          upstream_link_local_id = self.upstream_link_ids.index(global_upstream_link_id)
          turn_ratios = all_links[global_upstream_link_id].get_turn_ratios()
          for global_down_stream_link_index, tr in turn_ratios.items():
            if tr != 0:
              di = self.down_stream_link_ids.index(global_down_stream_link_index)
              self.local_alpha_beta_rate[key][
                upstream_link_local_id, di
              ] = all_links[global_down_stream_link_index].get_supply_ratio(key, global_upstream_link_id) / tr


  def get_indersection_for_ts(self):
    return (self.number, self._act_sets)
