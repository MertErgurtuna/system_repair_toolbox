import unittest
import network
import test_inputs
import numpy as np
from utilities import utilities


class TestNetwork(unittest.TestCase):
  def testConstructorOneLink(self):
    tn = network.Network(test_inputs.INPUT_DICTIONARY_ONE_LINK, [], scale=5)
    self.assertEqual(tn.get_link_count(), 1)
    self.assertEqual(tn.get_intersection_count(), 0)

  def testConstructorMultipleLinks(self):
    tn = network.Network(test_inputs.INPUT_LINK_DICTIONARY,
                         test_inputs.INPUT_INTERSECTION_DICTIONARY)

    self.assertEqual(tn.get_link_count(),3)
    self.assertEqual(tn.get_intersection_count(),1)

    self.assertEqual(tn.get_number_of_intersection_modes(), 2)
    self.assertEqual(tn.get_caps(), [10,12,8])

    self.assertEqual(tn.get_dependents(0), [{}, {'a':[1], 'b':[]}])
    self.assertEqual(tn.get_dependents(1), [{'a':[0], 'b':[2]}, {}])
    self.assertEqual(tn.get_dependents(2), [{}, {'a': [], 'b': [1]}])

    self.assertEqual(tn.get_external_input_ranges(0), [0, 2])
    self.assertEqual(tn.get_external_input_ranges(1), [0, 0])
    self.assertEqual(tn.get_external_input_ranges(2), [0, 1])

    # TODO test alpha beta matrices

  def test_network_example(self):
    tn = network.Network(test_inputs.INPUT_LINK_TEST_EXAMPLE_DICTIONARY,
                         test_inputs.INPUT_INTERSECTION_TEST_EXAMPLE_DICTIONARY)

    self.assertEqual(tn.get_number_of_intersection_modes(), 8)
    self.assertListEqual(tn.get_caps(), [6,4,4,4])
    link_intersection_0 = utilities.LinkIntersection(-1, 0)
    self.assertTupleEqual(tn.get_intersections_of_link(0), link_intersection_0)
    link_intersection_1 = utilities.LinkIntersection(0, 1)
    self.assertTupleEqual(tn.get_intersections_of_link(1), link_intersection_1)

    self.assertTupleEqual(tn.get_intersections_of_link(2), utilities.LinkIntersection(-1, 0))

    self.assertListEqual(tn.get_dependents(0), [{}, {'a': [1, 3], 'b': []}])
    self.assertListEqual(tn.get_dependents(1), [{'a': [0, 3], 'b': [2, 3]}, {'a': [], 'b': []}])

  def test_example_links(self):
    tn = network.Network(test_inputs.INPUT_LINK_TEST_EXAMPLE_DICTIONARY,
                         test_inputs.INPUT_INTERSECTION_TEST_EXAMPLE_DICTIONARY)

    link = tn.links[0]
    link_intersection_0 = utilities.LinkIntersection(-1, 0)
    self.assertListEqual(link.get_downstream_dependent_links(), [1, 3])
    self.assertListEqual(link.get_adjacent_links(), [])
    self.assertTupleEqual(link.get_intersections(), link_intersection_0)
    self.assertListEqual(link.get_downstream_dependent_links(), [1, 3])
    self.assertDictEqual(link.get_upstream_dependent_links(), {})
    self.assertEqual(link.get_supply_ratio('a', 1), 0)
    np.testing.assert_array_equal(link.get_decreasing_dep_mask(), np.array([0, 0, 0, 0]))
    np.testing.assert_array_equal(link.get_increasing_dep_mask(), np.array([1, 1, 0, 1]))

    link = tn.links[1]
    self.assertListEqual(link.get_downstream_dependent_links(), [])
    self.assertListEqual(link.get_adjacent_links(), [3])
    # for link
    # for network above.
    link_intersection_1 = utilities.LinkIntersection(0, 1)
    self.assertTupleEqual(link.get_intersections(), link_intersection_1)
    self.assertDictEqual(link.get_upstream_dependent_links(), {'a': [0], 'b':[2]})
    self.assertEqual(link.get_supply_ratio('a', 0), 1)
    self.assertEqual(link.get_supply_ratio('b', 2), 1)

    np.testing.assert_array_equal(link.get_decreasing_dep_mask(), np.array([0, 0, 0, 1]))
    np.testing.assert_array_equal(link.get_increasing_dep_mask(), np.array([1, 1, 1, 0]))


    # LINK 0, flow to 1 and 3
    # for i in range(0,8,2):
    #  np.testing.assert_array_equal(np_alpha_beta_rate_link0_a, np_to_test[i][0])

    # LINK 0, no flow
    #np_alpha_beta_rate_link0_b = np.ones(4)*np.inf
    #for i in range(1, 8, 2):
    #  np.testing.assert_array_equal(np_alpha_beta_rate_link0_b, np_to_test[i][0])

    # LINK 1/3, flow/noflow
    #np_alpha_beta_rate_link1_ab = np.ones(4)*np.inf
    #for i in range(0, 8, 1):
    #  np.testing.assert_array_equal(np_alpha_beta_rate_link1_ab, np_to_test[i][1])
    #  np.testing.assert_array_equal(np_alpha_beta_rate_link1_ab, np_to_test[i][3])

    # LINK 2, no flow a
    #np_alpha_beta_rate_link1 = np.ones(4)*np.inf
    #for i in range(0, 8, 2):
    #  np.testing.assert_array_equal(np_alpha_beta_rate_link1, np_to_test[i][2])
    # flow
    #np_alpha_beta_rate_link1[1] = 1/0.4
    #np_alpha_beta_rate_link1[3] = 1/0.6
    #for i in range(1, 8, 2):
    #  np.testing.assert_array_equal(np_alpha_beta_rate_link1, np_to_test[i][2])

    beta_matrix = [{} for _ in range(4)]# np.zeros((4, 4))
    beta_matrix[0][1] = 0.9
    beta_matrix[0][3] = 0.1
    beta_matrix[2][1] = 0.4
    beta_matrix[2][3] = 0.6
    for i in range(4):
      self.assertDictEqual(beta_matrix[i], tn.beta_matrix[i])

    # TODO TEST THE STEP FUNCTI0N AND COMPUTE NEXT FUNCTION
    # TODO: COMPUTE THOSE MATRICES ONLY FOR THE COMBINATIONS THAT AFFECT THE LINK ... be more efficient there.

  def test_example_intersections(self):
    tn = network.Network(test_inputs.INPUT_LINK_TEST_EXAMPLE_DICTIONARY,
                         test_inputs.INPUT_INTERSECTION_TEST_EXAMPLE_DICTIONARY)

    v = tn.get_intersection(0)
    self.assertEqual(v.local_alpha_beta_rate['a'].shape, (2,2))
    self.assertEqual(v.local_alpha_beta_rate['a'][0,0], 1.0/0.9) # 0 to 1
    self.assertEqual(v.local_alpha_beta_rate['a'][0,1], 1.0 / 0.1) # 0 to 3
    self.assertEqual(v.local_alpha_beta_rate['a'][1,0], np.inf) # 2 to 1 not enabled
    self.assertEqual(v.local_alpha_beta_rate['a'][1,1], np.inf) # 2 to 3 not enabled

    self.assertEqual(v.local_alpha_beta_rate['b'].shape, (2,2))
    self.assertEqual(v.local_alpha_beta_rate['b'][0, 0], np.inf)  # 0 to 1 not enabled
    self.assertEqual(v.local_alpha_beta_rate['b'][0, 1], np.inf)  # 0 to 3 not enabled
    self.assertEqual(v.local_alpha_beta_rate['b'][1, 0], 1.0/0.4)  # alpha 2 1 / beta 2 1 2 to 1 not enabled
    self.assertEqual(v.local_alpha_beta_rate['b'][1, 1], 1.0/0.6)  # 2 to 3 not enabled

  def test_compute_next_link(self):
    tn = network.Network(test_inputs.INPUT_LINK_TEST_EXAMPLE_DICTIONARY,
                         test_inputs.INPUT_INTERSECTION_TEST_EXAMPLE_DICTIONARY)

    l = 0
    np_xk = np.zeros(4)
    link_intersection = utilities.LinkIntersection(-1 , 'a')
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=4, local_intersection_mode=link_intersection), 4)
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=4,
                                          local_intersection_mode=utilities.LinkIntersection(-1 , 'b')), 4)
    np_xk[0] = 2
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=4,
                                          local_intersection_mode=utilities.LinkIntersection(-1 , 'a')), 4)
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=4,
                                          local_intersection_mode=utilities.LinkIntersection(-1 , 'b')), 6)

    self.assertEqual(tn.compute_next_link(l, np_xk, dl=5,
                                          local_intersection_mode=utilities.LinkIntersection(-1 , 'b')), 6)
    np_xk[1]=3
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=4,
                                          local_intersection_mode=utilities.LinkIntersection(-1 , 'a')),
                     6 - 1.0/0.9)
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=6,
                                          local_intersection_mode=utilities.LinkIntersection(-1 , 'b')), 6)

    self.assertEqual(tn.compute_next_link(l, np_xk, dl=5,
                                          local_intersection_mode=utilities.LinkIntersection(-1, 'b')), 6)
    l = 1
    np_xk = np.zeros(4)
    np_xk[l] = 2
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=0,
                                          local_intersection_mode=utilities.LinkIntersection('a', 'a')), 0)
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=0,
                                          local_intersection_mode=utilities.LinkIntersection('a', 'b')), 2)
    np_xk[0] = 2
    self.assertEqual(tn.compute_next_link(l, np_xk, dl=0,
                                          local_intersection_mode=utilities.LinkIntersection('a', 'a')), 1.8)


  def test_step(self):
    tn = network.Network(test_inputs.INPUT_LINK_TEST_EXAMPLE_DICTIONARY,
                         test_inputs.INPUT_INTERSECTION_TEST_EXAMPLE_DICTIONARY)
    np_xk = np.zeros(4)
    intersection_mode = 'aaa'
    np_xk_n = tn.step(np_xk,intersection_mode, test=True) # the second one is the dl
    np.testing.assert_array_equal(np_xk_n[0], np.array([1,0,0.5,0]))

    np_xk[0] = 2
    np_xk_n = tn.step(np_xk, intersection_mode, test=True)
    np.testing.assert_array_equal(np_xk_n[0], np.array([1, 2*0.9, 0.5, 2*0.1]))

    np_xk[1] = 4
    np_xk_n = tn.step(np_xk, intersection_mode, test=True)
    np.testing.assert_array_equal(np_xk_n[0], np.array([2+1, 4-3, 0.5, 0]))

    intersection_mode = 'bbb'
    np_xk[1] = 1
    np_xk[0] = 5.5
    np_xk[2] = 4
    np_xk[3] = 1
    np_xk_n = tn.step(np_xk, intersection_mode, test=True)
    np.testing.assert_array_equal(np_xk_n[0], np.array([6, 1 + 3*0.4, 1 + 0.5 , 1 + 3*0.6]))




if __name__ == '__main__':
  unittest.main()
