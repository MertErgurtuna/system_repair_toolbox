""" The class definitions used in the traffic network definition."""

import copy

import numpy as np
import functools

import intersection
import link
import offset_generator
from utilities import utilities


class TrafficNetworkError(Exception):
  pass


class Network(object):
  """The class representing the whole traffic network model."""

  def __init__(self, link_desc, intersection_desc, scale=2):
    """Constructs the traffic network from the given description.

    Args:
      desc: A list of dictionaries. Each dictionary describes a link and has the following fields:
        number:int capacity:float saturation_flow:float vup:int or vdown:int linkd:v or h flowdir:+ or -
    """
    self.links = []
    self._intersections = []
    self.np_xcap = np.zeros(len(link_desc))
    self.beta_matrix = [] # an array of cells to only store local information
    self.modes = []
    self._link_count = 0
    self.output_links = []
    self._intersection_mode_counts = []

    og = offset_generator.OffsetGenerator()
    og.set_scale(scale)

    for d in link_desc:
      self.links.append(link.Link(d, og, len(link_desc)))
      self.np_xcap[self._link_count] = self.links[self._link_count].get_cap()
      self._link_count += 1

    for l in self.links:
      try:
        l.check_ratios()
      except  TrafficNetworkError as err:
        print(err)

#    self.intersections = [intersection.Intersection(og, i) for i in intersection_ids]
    tmp_intersection_upstream_links = [[] for _ in range(len(intersection_desc))]
    tmp_intersection_downstream_links = [[] for _ in range(len(intersection_desc))]
    # For each intersection set their upstream and downstream links.
    for l in self.links:
      if l.vdown >= 0:
        tmp_intersection_upstream_links[l.vdown].append(
          {'number':l.number, 'link_dir':l.link_dir, 'flow_dir':l.flow_dir})
      if l.vup >= 0:
        tmp_intersection_downstream_links[l.vup].append(
          {'number':l.number, 'link_dir':l.link_dir, 'flow_dir':l.flow_dir})

    sorted_intersection_desc = sorted(intersection_desc, key=lambda k: k["number"])
    # using those links, create intersection objects.
    for i in range(len(sorted_intersection_desc)):
      self._intersections.append(intersection.Intersection(
        sorted_intersection_desc[i],
        tmp_intersection_upstream_links[i],
        tmp_intersection_downstream_links[i],
        og))
    for l in range(self._link_count):
      vup = self.links[l].get_vup()
      # If a link has an upstream intersection, it might have adjacent links.
      if vup != -1:
        adjacent_links = self._intersections[vup].get_downstream_links()
        adjacent_links.remove(l)
        self.links[l].set_adjacent_links(adjacent_links)

    # self._update_locations(150, 150)
    # self.set_fout_beta_rate_vectors()
    self.set_fout_beta_rate_vectors_local()
    self._intersection_mode_counts = [v.get_number_of_modes() for v in self._intersections]

  def _update_locations(self, x, y):
    # First find a road that does not have an upstream intersection.
    # l = next(l_ for l_ in self.links if l_.vup < 0)
    # l.move(x,y);

    # Trace all intersections, start from the first one:
    if len(self._intersections) == 0:
      return
    intersection_check = self._intersections[0].update_pos(x, y)
    i = 0
    while i < len(intersection_check):
      (d_or_v, l, xt, yt) = intersection_check[i]
      i += 1
      if d_or_v == 'd':
        vt = self.links[l].vdown
      else:
        vt = self.links[l].vup
      if vt > 0:
        intersection_check.extend(self._intersections[vt].update_pos(xt, yt))

    for l in self.links:
      l.update_pos(self._intersections[l.vup] if l.vup >= 0 else -1,
                   self._intersections[l.vdown] if l.vdown >= 0 else -1)

  def draw(self, win):
    for l in self.links:
      l.draw(win)
    for v in self._intersections:
      v.draw(win)

  def __str__(self):
    """Overload to string method."""
    output = "LINKS:\n%s\n INTERSECTIONS:\n%s\n" % (
    "\n".join(map(str, self.links)), "\n".join(map(str, self._intersections)))

    return output


  # TODO implement a post for a link (in the following post is computed for all, optimize it)
  def post_link(self, l, local_state_low_cell, local_state_up_cell, intersection_mode, dl_bounds=[0,0]):
    """Compute Post([x_low,x_up]) for the given region, where xl is the lower corner, xup is the upper corner. s.t.
       for all x in region R, x_low_i <= x_i <= x_up_i for external input range dl_low-d_up..

    Args:
      l: link index
      local_state_low_cell: A cell of the form: {link_index:value}.
      local_state_up_cell: A cell of the form: {link_index:value}.
      intersection_mode: A vector representing the mode of each intersection.
      dl_bounds: Lower and upper bound for the external input

    Returns a pair of lists post_x_low, post_x_up specifying the post for the link.
    """
    # Upper bound
    xk = copy.deepcopy(local_state_up_cell)
    for key in xk:
      if self.links[l].is_decreasing_dep(key):
        xk[key] = local_state_low_cell[key]

    if l == 6:
      2+1
    post_x_high = self.compute_next_link(l, xk, self.links[l].get_dl_max_mode(intersection_mode.v_up), intersection_mode)
    # xk = np.multiply(np_x_up, self.links[l].get_increasing_dep_mask()) + \
    #      np.multiply(np_x_low, self.links[l].get_decreasing_dep_mask())
    # xkn = self.compute_next(xk, dl_bounds[1], intersection_mode)
    # post_x_high = xkn[l]

    # Lower bound
    xk = copy.deepcopy(local_state_low_cell)
    for key in xk:
      if self.links[l].is_decreasing_dep(key):
        xk[key] = local_state_up_cell[key]
    post_x_low = self.compute_next_link(l, xk, self.links[l].get_min_dl(), intersection_mode)
    # xk = np.multiply(np_x_low, self.links[l].get_increasing_dep_mask()) + \
    #      np.multiply(np_x_up, self.links[l].get_decreasing_dep_mask())
    # xkn = self.compute_next(xk, dl_bounds[0], intersection_mode)
    # post_x_low = xkn[l]

    return post_x_low, post_x_high

  def compute_next_link(self, l, local_state_cell, dl, local_intersection_mode):
    """ Compute next for the given state and link id.

    Args:
      l:
      local_state_cell:
      dl:
      local_intersection_mode: link.LinkIntersection, a named tuple:
          local_intersection_mode.v_up, local_intersection_mode.v_down
          TODO: correct in test
    Returns the computed post value.
    """
    # _compute_fout_local(self, np_xk, intersection_mode, l):
    self_fout = self._compute_fout_local(local_state_cell, local_intersection_mode.v_down, l)
    v_up = self.links[l].vup
    if v_up == -1: # no upstream links
      return np.minimum(self.np_xcap[l], local_state_cell[l] - self_fout + dl)

    # otherwise compute the flow from the upstream links
    upstream_links = self.links[l].get_upstream_links(local_intersection_mode.v_up)
    flow_from_upstream_links = 0
    for us_l in upstream_links:
      us_l_f_out = self._compute_fout_local(local_state_cell, local_intersection_mode.v_up, us_l)
      flow_from_upstream_links += self.beta_matrix[us_l][l]*us_l_f_out
    return np.minimum(self.np_xcap[l], local_state_cell[l] - self_fout + dl + flow_from_upstream_links)

  def _compute_fout_local(self, local_state_cell, local_intersection_mode, l):
    """Compute fout for the given link."""
    # links downstream links
    l_f_out = min(local_state_cell[l], self.links[l].get_sat_flow())
    down_stream_intersection_id = self.links[l].vdown
    if down_stream_intersection_id < 0:
      return l_f_out

    if not self._intersections[down_stream_intersection_id].is_active(local_intersection_mode, l):
      return 0

    filter_ids = self._intersections[down_stream_intersection_id].down_stream_link_ids
    if not filter_ids:
      return l_f_out*self._intersections[down_stream_intersection_id].is_active(local_intersection_mode, l)
    np_xk_filtered = np.array([local_state_cell[ind] for ind in filter_ids])
    np_space = self.np_xcap[filter_ids] - np_xk_filtered
    local_rates = self._intersections[down_stream_intersection_id].get_local_alpha_beta_rates(local_intersection_mode, l)
    down_flow = np.inf
    for space, rate in zip(list(np_space), local_rates):
      if rate != np.inf:
        down_flow = min(down_flow, space*rate)

    l_f_out = min(l_f_out, down_flow)
    return l_f_out


  def initialize_links(self, np_xk, ratio):
    """
    Initialize upto ratio of capacities of each link
    Args:
      np_xk:
      ratio:
    """
    for i in range(self._link_count):
      np_xk[i] = self.links[i].initialize_link(ratio)

  def set_random_signal_mode(self, sm):
    for i in range(len(self._intersections)):
      sm[i] = self._intersections[i].get_random_mode()

  def step(self, np_xk, intersection_mode, test=False, printxn=False):
    """Compute the state at the next time step, and set to the links.

    Args:
      np_xk: A numpy array representing the state of each link at the current time step.
      intersection_mode: A vector representing the mode of each intersection.
      test:

    Returns: A numpy array representing the state of each link at the next time step.
    """
    # print "Step function"
    act_links = self._get_active_links(intersection_mode)
    if printxn:
      print("active links: " + " ".join(map(str, act_links)))
    np_dl = self._generate_input(intersection_mode, test)
    np_xk_1 = np.zeros(self._link_count)
    # dict of state:
    state_cell = {key: value for key, value in enumerate(list(np_xk))}
    for i in range(self._link_count):
      li = utilities.LinkIntersection(
        intersection_mode[self.links[i].get_vup()] if self.links[i].get_vup()>=0 else -1,
        intersection_mode[self.links[i].get_vdown()] if self.links[i].get_vdown() >= 0 else -1)
      np_xk_1[i] = self.compute_next_link(l=i,
                                          local_state_cell=state_cell,
                                          dl=np_dl[i],
                                          local_intersection_mode=li)
    if printxn:
      print("xn: " + " ".join(map(str, np_xk_1)) + " cap applied")
    self._set_states(np_xk_1, intersection_mode)
    return np_xk_1, np_dl

  def _set_states(self, xk, mode):
    for i in range(self._link_count):
      self.links[i].update(xk[i])
    # for vi, v in enumerate(self._intersections):
    #  v.update(mode[vi])

  def _generate_input(self,  intersection_modes, test=False):
    """Returns dl"""
    np_dl = np.zeros(self._link_count)
    for li,l in enumerate(self.links):
      np_dl[li] = l.generate_input(intersection_modes, test)
    return np_dl

  def _get_active_links(self, mode):
    active_links = []
    for v,m in zip(self._intersections, mode):
      active_links.extend(v.get_active_links(m))

    active_links.sort()
    return active_links

  def set_fout_beta_rate_vectors_local(self):
    """
    For each intersection, this method precomputes part of the fout function for each possible input mode.
    v.local_alpa_beta_rates = mode x upstream_links x downstream_links = alphalk/betalk

    """
    for v in self._intersections:
      v.set_local_alpha_beta_rates(self.links)
    self.beta_matrix = [{} for i in range(self._link_count) ]# np.zeros((self._link_count, self._link_count))
    for li,l in enumerate(self.links):
      tr = l.get_turn_ratios()
      for ti, t in tr.items():
        if t != 0:
          self.beta_matrix[li][ti] = t

  def get_link_count(self):
    return self._link_count

  def get_intersection_count(self):
    return len(self._intersections)

  def get_caps(self):
    """Returns a list of link capacities."""
    return [l.get_cap() for l in self.links]

  def get_number_of_intersection_modes(self):
    return functools.reduce(lambda x,y: x*y,  self._intersection_mode_counts)

  def get_intersection_mode_counts(self):
    return self._intersection_mode_counts

  def get_intersections_of_link(self, l):
    return self.links[l].get_intersections()

  def get_dependents(self, l):
    """Generates the set of links that effect the evolution of the link l with respect to the intersection
    configurations.

    Args:
      l: link number
    Returns:
      A list of cells of the form: [{'a': [], 'b': [1, 2]}, {'c': [], 'd': [1]}], 'a' and 'b' are the modes of the
      upstream intersection (if exists), 'c' and 'd' are the modes of the downstream intersection (if exists).
      The cell reads as
      when vup is in mode 'a', links [] affect the evolution of link l.
                          'b', links [1,2] affect the evolution of link l ....
    """
    vup = self.links[l].get_vup()
    if vup != -1:
      lup = self.links[l].get_upstream_dependent_links()
      # Get the adjacent links, i.e. downstream links of vup
      adjacent_links = self.links[l].get_adjacent_links()
      for key in lup:
        # append the adjacent links to the non-empty lists.
        if lup[key]:
          lup[key] += adjacent_links
    else: # place holder
      lup = {}


    vdown = self.links[l].get_vdown()
    if vdown != -1:
      ldown_list = self.links[l].get_downstream_dependent_links()
      ldown = self._intersections[vdown].get_modes_for_link(l)
      for key in ldown:
        if ldown[key]:
          ldown[key] = ldown_list
    else:
      ldown = {}
    # degraded
    # if vup != -1 and vdown != -1:
    #   if vdown < vup:
    #     return [ldown, lup]
    #   else:
    #     return [lup, ldown]
    # if vup != -1:
    #   return [lup]
    # if vdown != -1:
    #   return [ldown]
    return [lup, ldown]

  def get_external_input_ranges(self, l):
    return self.links[l].get_external_input_ranges()

  def get_intersection(self, ind):
    for v in self._intersections:
      if v.number == ind:
        return v

  def get_all_intersection_indices_and_modes(self):
    inds_modes = []
    for v in self._intersections:
      inds_modes.append(v.get_indersection_for_ts())
    return inds_modes







