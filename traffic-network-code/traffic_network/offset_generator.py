
class OffsetGenerator(object):
  """A class encapsulating the information about the direction of the links, and corresponding position offsets."""
  def __init__(self, s_=1):
    """Initialize the offsets for the given scale s_."""
    self._downstream_links = {'h-':'left', 'v-':'up', 'h+':'right', 'v+':'down'}
    self._upstream_links = {'h-':'right', 'v-':'down', 'h+':'left', 'v+':'up'}
    self._w = 3
    self._l = self._w * 6
    self._r = self._w * 1.2
    self._s = s_
    self._downstream_link_offsets = {}
    self._upstream_link_offsets = {}

    self._downstream_intersection_offsets = {}
    self._upstream_intersection_offsets = {}
    self._upstream_light_offsets = {}

  def _set_offsets(self):
    """Set the offsets for all for the offset."""
    self._downstream_link_offsets = {
      'h-': [(-(self._w + self._l + self._r), -self._r), (-self._r - self._w, self._w - self._r)], #5
      'v-': [(self._r - self._w, -(self._l + self._r + self._w)), (self._r, -self._w - self._r)],  # 6
      'h+': [(self._r + self._w, self._r-self._w), (self._r + self._w + self._l, self._r)],  # 7
      'v+': [(-self._r, self._r + self._w), (self._w - self._r, self._r + self._w + self._l)],  # 8
    }

    self._upstream_link_offsets = {
      'h-': [(self._r + self._w, -self._r ), (self._r + self._w + self._l, -self._r+ self._w)],  # 3
      'v-': [(self._r - self._w, self._r + self._w), (self._r, self._r + self._w + self._l)],  # 4
      'h+': [(-(self._w + self._l + self._r), self._r - self._w), (-self._r - self._w, self._r)],  # 1
      'v+': [(-self._r, -(self._l + self._r + self._w)), (self._w-self._r, -self._w - self._r)],  # 2
    }

    self._upstream_light_offsets = {
      'h-': [self._r + self._w/2, -self._r + self._w/2],
      'v-': [self._r - self._w/2, self._r + self._w/2],
      'h+': [-self._r - self._w/2, self._r - self._w/2],
      'v+': [-self._r + self._w/2,  -self._r - self._w/2],  # 2
    }
    for k in self._downstream_link_offsets:
      for j in range(len(self._downstream_link_offsets[k])):
        self._downstream_link_offsets[k][j] = (
          self._downstream_link_offsets[k][j][0] * self._s,
          self._downstream_link_offsets[k][j][1] * self._s)
    for k in self._upstream_link_offsets:
      for j in range(len(self._upstream_link_offsets[k])):
        self._upstream_link_offsets[k][j] = (
          self._upstream_link_offsets[k][j][0] * self._s,
          self._upstream_link_offsets[k][j][1] * self._s)
    for k in self._upstream_light_offsets:
      for i in range(len(self._upstream_light_offsets[k])):
        self._upstream_light_offsets[k][i] = self._upstream_light_offsets[k][i]*self._s

    m = self._s * (self._r * 2 + self._w * 2 + self._l)
    self._downstream_intersection_offsets = {'h-': (-m,0), 'v-': (0,-m), 'h+':(m,0), 'v+':(0,m)}
    self._upstream_intersection_offsets =  {'h-': (m,0), 'v-': (0,m), 'h+':(-m,0), 'v+':(0,-m)}

  def get_radius(self):
    """Returns the radius."""
    return self._r * self._s

  def set_scale(self, s):
    """Sets the radius to s."""
    self._s = s
    self._set_offsets()

  def get_dir_name(self, is_upstream, link_dir, flow_dir):
    """Returns the name of the direction from _dir."""
    if is_upstream:
      return self._upstream_links.get(link_dir+flow_dir, 'no_up_error'+link_dir+flow_dir);
    else:
      return self._downstream_links.get(link_dir+flow_dir, 'no_down_error'+link_dir+flow_dir);

  def get_light_width(self):
    """Returns width."""
    return self._w * 1.2

  def get_offsets(self, is_upstream, link_dir, flow_dir):
    """Returns the scaled offset for the given direction."""
    if is_upstream:
      return self._upstream_link_offsets.get(link_dir+flow_dir, [(0,0),(0,0)]);
    else:
      return self._downstream_link_offsets.get(link_dir+flow_dir, [(0,0),(0,0)]);

  def get_intersection_offsets(self, is_upstream, link_dir, flow_dir):
    """Returns the scaled offset for the given direction for the next intersection."""
    if is_upstream:
      return self._upstream_intersection_offsets.get(link_dir+flow_dir, (0,0));
    else:
      return self._downstream_intersection_offsets.get(link_dir+flow_dir, (0,0));

  def get_light_offset(self, link_dir, flow_dir):
    """Returns the intersection light offsets."""
    return self._upstream_light_offsets.get(link_dir+flow_dir, (0,0));
