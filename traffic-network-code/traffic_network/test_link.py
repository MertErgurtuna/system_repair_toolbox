import link
import unittest


def _setup_link():
  # SETUP
  link_count = 2
  desc = {'number': 0,
          'capacity': 10,
          'saturation_flow': 5,
          'dl_min': 4,
          'dl_max': 12,
          'vup': 0,
          'vdown': -1,
          'linkdir': 'v',
          'flowdir': '+',
          'supply_ratios': {'a': [(1, 0.4)], 'b': [(1, 0)]}}
  og = 0
  l = link.Link(desc, og, link_count)
  return l

class TestLink(unittest.TestCase):
  def test_init(self):
    l = _setup_link()
    self.assertTrue(isinstance(l, link.Link))

  def test_get_cap(self):
    l = _setup_link()
    self.assertEqual(l.get_cap(), 10)

  def test_get_number(self):
    l = _setup_link()
    self.assertEqual(l.get_number(), 0)

  def test_get_sat_flow(self):
    l = _setup_link()
    self.assertEqual(l.get_sat_flow(),5)

  def test_get_dl(self):
    l = _setup_link()
    v1 = l.generate_input(0, True)
    self.assertEqual(v1, 8)
    v2 = l.generate_input({0:'a', 1:'a'})
    self.assertLessEqual(v2, 12)
    self.assertGreaterEqual(v2, 4)

  def test_get_link_desc(self):
    l = _setup_link()
    self.assertEqual(l.get_link_desc(), 'v+')

  def test_get_supply_ratio(self):
    l = _setup_link()
    self.assertEqual(l.get_supply_ratio('a', 1), 0.4)
    self.assertEqual(l.get_supply_ratio(['a', 'b'], 1), 0.4)

if __name__ == '__main__':
  unittest.main()
