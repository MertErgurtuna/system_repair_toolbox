INPUT_DICTIONARY_ONE_LINK = [
  {'flowdir': '+', 'dl_min': 0.0, 'capacity': '1', 'saturation_flow': '1', 'linkdir': 'h',
   'number': 0, 'dl_max': 1.0}]


INPUT_LINK_DICTIONARY = [
  {'flowdir': '+', 'dl_min': 0.0, 'capacity': '10', 'saturation_flow': '5', 'linkdir': 'h', 'vdown': 0,
   'number': 0, 'dl_max': 2.0, 'turn_ratios': [(1, 0.9)]},
  {'output_link': True, 'flowdir': '+', 'dl_min': 0.0,'capacity': '12', 'saturation_flow': '6', 'linkdir': 'h',
   'number': 1, 'dl_max': 0.0, 'supply_ratios': {'a': [(0, 1.0)], 'b': [(2, 1.0)]}, 'vup': 0},
  {'flowdir': '+', 'dl_min': 0.0, 'capacity': '8', 'saturation_flow': '4', 'linkdir': 'v', 'vdown': 0, 'number': 2,
   'dl_max': 1.0, 'turn_ratios': [(1, 0.8)]}]

INPUT_INTERSECTION_DICTIONARY = [{'act_set': {'a': [0], 'b': [2]}, 'number': 0}]
INPUT_INTERSECTION_DICTIONAR_UP_DS = [[[0, 2], [1]]]

# TEST INPUTS FROM SAMPLE FILES, IF YOU CHANGE THEM UPDATE THOSE ACCORDINGLY
INPUT_LINK_TEST_EXAMPLE_DICTIONARY = [
  {'flowdir': '+', 'dl_min': 0.0, 'capacity': '6', 'saturation_flow': '4', 'linkdir': 'h', 'vdown': 0, 'number': 0,
   'dl_max': 2.0, 'turn_ratios': [(1, 0.9), (3, 0.1)]},
  {'output_link': True, 'flowdir': '+', 'dl_min': 0.0, 'capacity': '4', 'saturation_flow': '3', 'linkdir': 'h',
   'vdown': 1, 'number': 1, 'dl_max': 0.0, 'supply_ratios': {'a': [(0, 1.0)], 'b': [(2, 1.0)]}, 'vup': 0,
   'turn_ratios': []},
  {'flowdir': '+', 'dl_min': 0.0, 'capacity': '4', 'saturation_flow': '3', 'linkdir': 'v','vdown': 0, 'number': 2,
   'dl_max': 1.0, 'turn_ratios': [(1, 0.4), (3, 0.6)]},
  {'output_link': True, 'flowdir': '+', 'dl_min': 0.0,'capacity': '4', 'saturation_flow': '3', 'linkdir': 'v',
   'vdown': 2, 'number': 3, 'dl_max': 0.0, 'supply_ratios': {'a': [(0, 1.0)], 'b': [(2, 1.0)]}, 'vup': 0,
   'turn_ratios': []}]
INPUT_INTERSECTION_TEST_EXAMPLE_DICTIONARY = [
  {'act_set': {'a': [0], 'b': [2]}, 'number': 0},
  {'act_set': {'a': [1], 'b': []}, 'number': 1},
  {'act_set': {'a': [3], 'b': []}, 'number': 2}]

