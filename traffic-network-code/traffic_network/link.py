import numpy as np

import utilities as u
#from external import graphics as g
#from external import graphics2 as g
import random


class LinkError(Exception):
  pass

class _color(object):
  def __init__(self, r, g, b):
    self.r = r
    self.g = g
    self.b = b

def get_color(r):
  if r < 0.6:
    return _color(0,int(255*( (1-r)*0.625 + 0.375)),0)
  elif r < 0.8:
    return _color(int(255*((1-r) + 0.6)), int(255*((1-r) + 0.6)), 0)
  return _color(int(255* ((1-r)*2.5 + 0.5)),0,0)

link_colors = [_color(0,255,0), _color(0,128,0), _color(255,255,0), _color(255,0,0)]


class Link(object):
  """The class representing a link (road) in a traffic network model."""

  def __init__(self, desc, og_, link_count):
    """The constructor.
      Args:
        desc: A dictionary with the following link properties:
              number:int capacity:float saturation_flow:float vup:int or vdown:int linkd:v or h flowdir:+ or -
        og_: An OffsetGenerator object (see offset_generator.py, for drawing).
        link_count: The number of links in the network this link belongs to.
    """
    self.og = og_
    self.number = desc['number'] # number, id for the link
    self._cap = int(desc.get('capacity', 40))
    self._cl = int(desc.get('saturation_flow', 10))
    self.dl = [desc.get('dl_min', 0), desc.get('dl_max', 0)]
    self.dlmax = {}
    self.dlmax['a'] = desc.get('dl_max_a', 0)
    self.dlmax['b'] = desc.get('dl_max_b', 0)

    self.vup = desc.get('vup', -1)
    self.vdown = desc.get('vdown', -1)
    self.intersections = u.LinkIntersection(self.vup, self.vdown)
    self.link_dir = desc['linkdir']
    self.flow_dir = desc['flowdir']
    # self._output_link = desc.get("output_link", False)

    self._turn_ratios = {} # link_id, ratio pair
    self._supply_ratios = {}
    self.x = []
    #self.text =  g.Text(g.Point(0, 0), str(self.number))
    if  "supply_ratios" in desc:
      for key, value in desc["supply_ratios"].items():
        self._supply_ratios[key] = {} #np.zeros(link_count)
        for (l,r) in value:
          self._supply_ratios[key][l] = r

    # Compute downstream, upstream, dependencies.
    self._down_stream_links = []
    self._adjacent_links = []
    for (l, r) in desc.get("turn_ratios", []):
      self._turn_ratios[l] = r
      self._down_stream_links.append(l)
    self._down_stream_links = sorted(set(self._down_stream_links))
    self._np_increasing_dep_mask = np.zeros(link_count)
    # down stream links has increasing relation
    for i in self._down_stream_links:
      self._np_increasing_dep_mask[i] = 1
    # upstream links has increasing relation
    for key in self._supply_ratios:
      for key_index in self._supply_ratios[key]:
        self._np_increasing_dep_mask[key_index] = 1
    # increasing relation with itself
    self._np_increasing_dep_mask[self.number] = 1
    # the link has decreasing relation with adjacent links, the mask will be set up in network.py
    self._np_decreasing_dep_mask = np.zeros(link_count)
    # self.shape = g.Rectangle(g.Point(0, 0), g.Point(32, 2))

  def set_adjacent_links(self, adjacent_links):
    self._adjacent_links = adjacent_links # also decreasing dependency list
    for i in adjacent_links:
      self._np_decreasing_dep_mask[i] = 1

  def get_increasing_dep_mask(self):
    return self._np_increasing_dep_mask

  def get_decreasing_dep_mask(self):
    return self._np_decreasing_dep_mask

  def __str__(self):
    """To string overload, used for printing."""
    return "ID: %d, capacity: %d, cl: %d, dl: [%d %d], v_up: %d %s, v_down: %d %s, Turn ratios %s, Supply ratios %s, Location %s-%s" % (
      self.number,
      self._cap,
      self._cl,
      self.dl[0],
      self.dl[1],
      self.vup,
      self.og.get_dir_name(True, self.link_dir, self.flow_dir),
      self.vdown,
      self.og.get_dir_name(False, self.link_dir, self.flow_dir),
      utilities.dict_to_string(self._turn_ratios),
      " ".join([ key + ":" + utilities.dict_to_string(values) for key, values in self._supply_ratios.items()]),
      utilities.str_point(self.shape.p1),
      utilities.str_point(self.shape.p2))

  def check_ratios(self):
    """Check the ratios, sum of either ratios should not be greater than 1."""
    x = sum([sum(v.values()) > 1 for v in self._supply_ratios.values()])
    x = 1 if x > 1 else 0
    x += (2 if sum(self._turn_ratios.values()) > 1 else 0)
    ratio_check_result = ["OK", "Error in supply ratios", "Error in turn ratios",
                          "Error in both supply and turn ratios"]
    if x > 0:
        raise LinkError(ratio_check_result[x])


  def update_pos(self, v_up, v_down):
    if v_up != -1:
      p = v_up.get_center()
      (p1, p2) = self.og.get_offsets(False, self.link_dir, self.flow_dir)
      self.shape = g.Rectangle(g.Point(p[0] + p1[0], p[1] + p1[1]), g.Point(p[0] + p2[0], p[1] + p2[1]))
    elif v_down != -1:
      p = v_down.get_center()
      (p1, p2) = self.og.get_offsets(True, self.link_dir, self.flow_dir)
      self.shape = g.Rectangle(g.Point(p[0] + p1[0], p[1] + p1[1]), g.Point(p[0] + p2[0], p[1] + p2[1]))
    p = self.shape.getCenter()
    self.text.move(p.x, p.y)

  def update(self, x):
    """Update the color."""
    self.x = x
    #r = self.x/self._cap
    #i = min(3, int(4 * self.x / self._cap))
    #color_local = get_color(r)
    #color = g.color_rgb(color_local.r, color_local.g,color_local.b)
    #self.shape.setFill(color)
    #tmp = str(self.number) + ("\n" if self.link_dir == "v" else ":")
    #tmp += str(r)[0:3]
    #self.text.setText(tmp)

  def draw(self, win):
    """Draw the shape and add a label."""
    self.shape.draw(win)
    self.text.draw(win)

  def initialize_link(self, ratio):
    return self._cap*ratio*random.random()

  def get_link_desc(self):
    """Returns link_dir + flow_dir, e.g., h+."""
    return self.link_dir+self.flow_dir

  def get_cap(self):
    return self._cap

  def get_sat_flow(self):
    return self._cl

  def get_supply_ratio(self, mode, k):
    if isinstance(mode, list):
      if self.vup >= 0:
        mode = mode[self.vup]
      else:
        return 0
    if mode in self._supply_ratios:
      return self._supply_ratios[mode].get(k, 0)
    else:
      return 0

  def get_turn_ratios(self):
    return self._turn_ratios

  def generate_input(self, intersection_modes, test = False):
    if test:
      return 0.5*(self.dl[1] - self.dl[0]) + self.dl[0]
    else:
      dm = self.dl[1]

      if self.intersections.v_up != -1:
        dm += self.dlmax[intersection_modes[self.intersections.v_up]]

      if dm > 0:
        return np.random.random_sample()/2 + dm - 0.5

      return self.dl[1]
      #return np.random.random_sample()*(self.dl[1] - self.dl[0]) + self.dl[0]

  def get_number(self):
    return self.number

  def get_upstream_links(self, key):
    return self._supply_ratios[key].keys()

  def get_upstream_dependent_links(self):
    upstream_links = {}
    for key in self._supply_ratios:
      upstream_links[key] = self._supply_ratios[key].keys()
    return upstream_links

  def get_downstream_dependent_links(self):
    return self._down_stream_links

  def get_intersections(self):
    #return sorted((([self.vup] if self.vup != -1 else []) + ([self.vdown] if self.vdown != -1 else [])))
    return self.intersections

  def get_vup(self):
    return self.vup

  def get_vdown(self):
    return self.vdown

  def get_max_dl(self):
    return self.dl[1]

  def get_min_dl(self):
    return self.dl[0]

  def get_external_input_ranges(self):
    return self.dl

  def get_adjacent_links(self):
    return self._adjacent_links

  def is_decreasing_dep(self, l):
    return self._np_decreasing_dep_mask[l]

  def get_dl_max_mode(self, mode):
    if self.vup != -1:
        return self.dl[1] + self.dlmax[mode]
    else:
      return self.dl[1]


