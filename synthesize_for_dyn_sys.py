from optimization import diagonal_search
from trace_checker import formula_generator
import sys

def synthesize_repairable_formulas_for_dyn_sys(only_x1):

  if only_x1 == 1:
    folder_name = 'test_data/traffic_data/l6system/'
    bound = 30
    parameter_domains = {'p0': range(1, 30, 2),
                         'p1': range(1, 30, 2),
                         'p2': range(1, 30, 2),
                         'p3': range(1, 16, 2),
                         'p4': range(1, 16, 2),
                         'p5': range(1, 16, 2),
                         'p6': range(0, 2, 1),
                         'p7': range(0, 2, 1),
                         'p8': range(1, 5, 1),
                         'p9': range(0, 2, 1),
                         'pP': range(0, 5, 1),
                         'pA': range(0, 5, 1)
                         }
  else:
    folder_name = 'test_data/traffic_data/l6system_allroads/'
    bound = 150
    parameter_domains = {'p0': range(1, 30, 2),
                       'p1': range(1, 30, 2),
                       'p2': range(1, 30, 2),
                       'p3': range(1, 16, 2),
                       'p4': range(1, 16, 2),
                       'p5': range(1, 16, 2),
                       'p6': range(0, 2, 1),
                       'p7': range(0, 2, 1),
                       'p8': range(2, 5, 1),
                       'p9': range(0, 2, 1),
                       'pP': range(0, 5, 1),
                       'pA': range(0, 5, 1)
                       }

  signal_file_base = 'test'
  trace_count = 20
  pc = 48 # processor count

  metric_list = ['0', '1', '2', '3', '4', '5', '6', '7']
  set_valued_metrics = ['6', '7']
  oc = [0, 1]
  formula_list = []
  for o_c in oc:
      template_formula_list = formula_generator.generate_formula_tree_iterative(metric_list=metric_list,
                                                                                operator_count=o_c,
                                                                                return_formula_string=True,
                                                                                set_valued_metrics=set_valued_metrics,
                                                                                withoutS=False)
      for form in template_formula_list:
          formula_list.append(form)
  formula_list = [formula for formula in formula_list if formula.split().count('S') < 1]
  formula_list = [formula for formula in formula_list if formula.split().count('|') < 1]
  template_repairable_formula_list = []
  for formula in formula_list:
      modified_formula = " ( A 1 p8 ( x6 =  p9 ) ) & ( P 1 1 ( " + formula + " ) ) "
      template_repairable_formula_list.append(modified_formula)
      modified_formula = " ( A 1 p8 ( x7 =  p9 ) ) & ( P 1 1 ( " + formula + " ) ) "
      template_repairable_formula_list.append(modified_formula)

  max_d = 5
  diagonal_search.search_template_formulas(template_repairable_formula_list, folder_name, signal_file_base, trace_count, pc, parameter_domains,  bound, max_d, log_file_name="log", verbose=True)

if __name__ == '__main__':
    only_x1 = int(sys.argv[1])
    synthesize_repairable_formulas_for_dyn_sys(only_x1)




