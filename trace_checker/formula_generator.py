
from . import formula_utilities as U
from . import STL
from optimization import formula_search
import itertools

#from optimization.formula_search import return_list_of_parameter_formula_from_template
# See tests for the details of this class.


def check_idempotence(op, lf, rf, set_valued_metrics):
  #check for strict equality between two formulas
  if set_valued_metrics and op == '|': #(x = 3) | (x = 4) is valid
    return True

  if lf.to_formula() == rf.to_formula():
    return False

  return True

def check_associativity(op, lf, rf):
  #Left-associativity
  #If a new formula will be constructed with the latest used operator it is added to the front.
  #Allows formulas in the form of (p | q) | r but not p | (q | r)

  if rf.op == op:
    return False
  
  return True

def check_repeated_unary(op, lf):
  #if two unary operators are consecutively added it is redundant since there exists a formula with less operators
  # !!(x0 > p0) = (x0 > p0)
  # P 0 pP (P 0 pP (x0 > p0)) = P 0 pP (x0 > p0) it is also true for A 
  if lf.op == op:
    return False
  
  return True

def check_absorption(op, lf, rf):

  #Absorption law states : p | (p & q) = p and p & (p | q) = p
  #This function checks the law
  lf_formula = lf.to_formula()
  rf_formula = rf.to_formula()

  if U.is_binary(rf.op) and U.is_boolean(rf.op):
    if lf_formula == rf.left_node.to_formula() or lf_formula == rf.right_node.to_formula():
      return False

  if U.is_binary(lf.op) and U.is_boolean(lf.op):
    if rf_formula == lf.right_node.to_formula() or rf_formula == lf.left_node.to_formula():
      return False
  
  return True

def check_distributivity(op, lf, rf):

  #Checks for distrubitive law i.e. (p & q) | (p & r) = p & (q | r)

  if lf.op == rf.op and U.is_boolean(lf.op) and U.is_binary(lf.op):
    #check if it is in the form (p & q) | (r & t)
    left1 = lf.left_node.to_formula() #p
    left2 = lf.right_node.to_formula() #q
    right1 = rf.left_node.to_formula() #r
    right2 = rf.right_node.to_formula() #t

    #if any one of p or q is equal to any one of r or t it has a equivalent formula with less operators
    if left1 == right1 or left1 == right2 or left2 == right1 or left2 == right2:
      return False

  return True

def check_de_morgan(op, lf, rf):
  #controls if applying de Morgan's Rule simplifies the formula 

  #Checks !(p & !q) => !p | q
  #If the operator is !, previous operator is & or |, and if the sub_formulas start with !, then that formula has an equivalent with less operators
  if op == '!':
    if U.is_binary(lf.op) and U.is_boolean(lf.op):
      if lf.right_node.op == '!' or lf.left_node.op == '!':
        return False
  
  #Checks the other direction of the rule: !p & !q => !(p | q)
  elif U.is_binary(op) and U.is_boolean(op):
    if lf.op == rf.op and lf.op == '!':
      return False
  
  return True

def check_not_conditions(op, lf, set_valued_metrics):

  #If the negation is followed by a 0 operator formula without the operand '=', that formula is redundant
  # !(x0 > p0) is roughly equivalent to (x0 < p0) for our purposes 
  if U.is_operand(lf.op) and not set_valued_metrics: 
    return False

  #If the negation is followed by another unary operator then it is again redundant
  #!(P 0 pP (x0 > p0) = A 0 pA ( !(x0 > p0)) and similarly !(A 0 pA (x0 > p0) = P 0 pP ( !(x0 > p0))
  if U.is_unary(lf.op) :
    return False

  return True

def check_redundancy(op, lf, rf = None, set_valued_metrics = []):
   
    #returns true if it is valid to construct the larger formula with given sub-formulas and operator
    #if lf and rf are equivalent returns false

    if U.is_binary(op) and U.is_boolean(op): #and,or
        return (check_idempotence(op, lf, rf, set_valued_metrics) and #checks strict equality
                check_associativity(op, lf, rf) and #checks for associativity rule
                check_absorption(op, lf, rf) and #checks for absorption rule
                check_distributivity(op, lf, rf) and  #checks distrubitivity rule
                check_de_morgan(op, lf, rf) ) #checks one direction of de Morgan
    
    if U.is_unary(op):
        if U.is_boolean(op): #not
            return (check_not_conditions(op, lf, set_valued_metrics) and #checks the cases for operator !
                    check_de_morgan(op,lf,rf) ) #checks reverse direction of de Morgan 
        
        return check_repeated_unary(op, lf) #Checks the repeated unary case
    return True

def generate_formula_tree_iterative(metric_list, operator_count, return_formula_string, set_valued_metrics=[], simplify=True, withoutS=False):
  """
  :param withoutS: (bool) if True, the formulas are generates without Since.
  """

  return_list = []
  for m in set_valued_metrics: # only equality
    stn = STL.SyntaxTreeNode()
    stn.op = '='
    stn.param = 'p' + str(m)
    stn.metric = m
    return_list.append(stn)

  for m in metric_list:
    if not m in set_valued_metrics:
      for op in ['>', '<']:
        stn = STL.SyntaxTreeNode()
        stn.op = op
        stn.param = 'p' + str(m)
        stn.metric = m
        return_list.append(stn)

  if operator_count == 0:
    if return_formula_string:
      return [t.to_formula() for t in return_list]
    return return_list

  #all_trees[operator_count] will be the final list of formulas
  #now only includes formulas with oc = 0
  all_trees = {}
  all_trees[0] = return_list

  if withoutS:
    operator_list = ['A', 'P'] + U.BooleanOperators
  else:
    operator_list = U.STLPastOperators + U.BooleanOperators

  for oc in range(1, operator_count+1):
    all_trees[oc] = []

    for op in operator_list:
      if U.is_binary(op):
        lc_count = oc
        if U.is_boolean(op):
          lc_count = 1 + int(oc/2)
        
        for lc in range(lc_count):
          for li in range(len(all_trees[lc])): # for li in range(len(all_trees[lc]))
            for ri in range(len(all_trees[oc-lc-1])): #for ri in range(len(all_trees[lc]))
              if simplify == True and U.is_boolean(op) and lc == oc-lc-1 and li < ri:
                break  #covers the commutative case, i.e. (p & q) is allowed but (q & p) is not allowed

              if simplify != True or check_redundancy(op,all_trees[lc][li], all_trees[oc-lc-1][ri]) == True:
                stn = STL.SyntaxTreeNode()
                stn.op = op
                stn.left_node = all_trees[lc][li]
                stn.right_node = all_trees[oc-lc-1][ri]
                all_trees[oc].append(stn)

      else:
        for lf in all_trees[oc - 1]:
          if simplify != True or check_redundancy(op,lf) == True:
            stn = STL.SyntaxTreeNode()
            stn.op = op
            stn.left_node = lf
            all_trees[oc].append(stn)

  return_list = all_trees[operator_count]

  if return_formula_string:
    return [t.to_formula() for t in return_list]
  return return_list


def generate_formula_tree(metric_list, operator_count, return_formula_string, set_valued_metrics=[]):

  return_list = []
  if operator_count == 0:
    for m in set_valued_metrics: # only equality
      stn = STL.SyntaxTreeNode()
      stn.op = '='
      stn.param = 'p' + str(m)
      stn.metric = m
      return_list.append(stn)

    for m in metric_list:
      if not m in set_valued_metrics:
        for op in ['>', '<']:
          stn = STL.SyntaxTreeNode()
          stn.op = op
          stn.param = 'p' + str(m)
          stn.metric = m
          return_list.append(stn)

    # for op in U.MetricOperators:
    #   for m in metric_list:
    #     stn = STL.SyntaxTreeNode()
    #     stn.op = op
    #     stn.param = 'p' + str(m)
    #     stn.metric = m
    #     return_list.append(stn)

    if return_formula_string:
      return [t.to_formula() for t in return_list]
    return return_list

  # Compute all with less number of operators:
  all_trees = {}
  for oc in range(operator_count):
    all_trees[oc] = generate_formula_tree(metric_list, oc, False, set_valued_metrics)

  # now combine them
  for op in U.BooleanOperators + U.STLPastOperators:

    if U.is_binary(op):
      # For symmetric operators like and/or, only go through the half
      lc_count = operator_count
      if U.is_boolean(op):
        lc_count = 1 + int(operator_count/2)
      for lc in range(lc_count):
        for lf in all_trees[lc]:
          for rf in all_trees[operator_count - lc - 1]:
            stn = STL.SyntaxTreeNode()
            stn.op = op
            stn.left_node = lf
            stn.right_node = rf
            return_list.append(stn)

    else:
      for lf in all_trees[operator_count - 1]:
        stn = STL.SyntaxTreeNode()
        stn.op = op
        stn.left_node = lf
        return_list.append(stn)

  if return_formula_string:
    return [t.to_formula() for t in return_list]
  return return_list


def generate_formula_tree_for_cause_mining(metric_list, control_metrics, operator_count, set_valued_metrics, parameter_domains, withoutS=False):
  """
  generate_formula_tree_for_cause_mining returns STL formulas in the form:
  \Phi = P 1 1 ( \phi^{u} and \phi^{x,u} ) where \phi^{u} is any pastSTL formula containing only control inputs and
  \phi^{x,u} is any pastSTL formula containing both system variables and control inputs.
  Note that if \phi^{u} is controllable (i.e. there is a perfect control strategy - a sequence of control choices -
  that violates \phi^{u} at each time step, then \Phi is also controllable.

  Args:
    metric_list: list of metrics that will appear in formulas
    control_metrics: the metrics that we can control (set valued)
    operator_count: how many operators will be in the resulting formula between metrics. If type(operator_count) = int,
    then we take the operator count of \phi^{u} to be 0, and he operator count of \phi^{x,u} to be operator_count.
    Else, we assume it is a two-element list, tuple or set and (say it contains (m,n) ) set the operator count of
    \phi^{u} (we call it lhs) to be m, and the operator count of \phi^{x,u} (rhs) to be n.
    set_valued_metrics: metrics that take discrete values from a predefined set. All control inputs are set valued.
    parameter_domains: used to eliminate the uncontrollable formulas (with checking only the lhs with all the possible
    parameters)
    withoutS: (bool) if True, the formulas are generated without Since.
  Returns:
      list of generated formulas

  A reminder for today's dummy scenario: In our dummy traffic system case(06.2018), all control metrics are set_valued,
  and they are for now called x5 and x6, so the formula is of the form:
  (formula with one op) is in the form x5 = {0,1} or x6 = {0,1}, i.e. it contains traffic light info
"""
  # Save parameter_domains in a different format to give to formula_is_conrollable
  related_parameter_domains = dict()  # parameter domains including inputs only and not time parameters, also 'p's are turned into 'x's
  for k, v in parameter_domains.copy().items():
    if not (k[1] == 'A' or k[1] == 'P' or k[1] == 'S' or k[1] == 'T'):  # so it is not a time parameter
      related_parameter_domains['x' + k[1:]] = v

  if type(operator_count)is int:
    operator_count_lhs, operator_count_rhs = [0, operator_count]
  else:
    operator_count_lhs, operator_count_rhs = operator_count

  template_lhs_formula_list = []  # Lhs formulas (p^{u}) without any parameters, just as templates
  parameter_lhs_formula_list = []  # All lhs template formulas with parameters
  controllable_lhs_formula_list = []  # Controllable formulas amongst parameter_lhs_formula_list
  return_list = []  # controllable lhs formulas concatanated with template rhs formulas (altogether a controllable formula now)

  if operator_count == -1:
    formulas_inside = generate_formula_tree_iterative(metric_list=control_metrics, operator_count=operator_count_lhs,
                                                      return_formula_string=False, set_valued_metrics=set_valued_metrics,
                                                      withoutS=withoutS)
    for frml in formulas_inside:
      stn = STL.SyntaxTreeNode()
      stn.op = 'P'
      stn.interval = STL.Interval(1, 1)
      stn.left_node = frml
      template_lhs_formula_list.append(stn.to_formula())

    # Generate parameters for each formula in formula_list
    for template_lhs_formula in template_lhs_formula_list:
      parameter_lhs_formula_list += generate_parametrized_formulas(template_lhs_formula, parameter_domains)

    control_inputs = ['x'+i for i in control_metrics]
    # Check which ones are controllable
    for lhs_formula in parameter_lhs_formula_list:
      if formula_is_controllable(lhs_formula, related_parameter_domains, control_inputs):
        controllable_lhs_formula_list.append(lhs_formula)

    # Return the controllable ones with the corresponding parameters
    return_list = controllable_lhs_formula_list

  else:
    formulas_left_side = generate_formula_tree_iterative(metric_list=control_metrics, operator_count=operator_count_lhs,
                                                         return_formula_string=True,
                                                         set_valued_metrics=set_valued_metrics,withoutS=withoutS)
    # Generate parameters for each formula in formulas_left_side
    for template_lhs_formula in formulas_left_side:
      parameter_lhs_formula_list += generate_parametrized_formulas(template_lhs_formula, parameter_domains)

    # Check which ones are controllable
    control_inputs = ['x'+i for i in control_metrics]
    for formula in parameter_lhs_formula_list:
      if formula_is_controllable(formula, related_parameter_domains, control_inputs):
        controllable_lhs_formula_list.append(formula)
    if not controllable_lhs_formula_list:
      return return_list

    formulas_right_side = generate_formula_tree_iterative(metric_list=metric_list, operator_count=operator_count_rhs,
                                                          return_formula_string=False,
                                                          set_valued_metrics=set_valued_metrics,withoutS=withoutS)


    # Concatanate the controllable lhs parameter formulas with rhs formula templates
    for frml_left_side in controllable_lhs_formula_list:
      frml_lhs_prefix = STL.infix_to_prefix(frml_left_side)
      stn_lhs = STL.SyntaxTreeNode()
      stn_lhs.initialize_node(frml_lhs_prefix.split(), 0)
      for frml_right_side in formulas_right_side:
        stn_inside = STL.SyntaxTreeNode()
        stn_inside.op = '&'
        stn_inside.left_node = stn_lhs
        stn_inside.right_node = frml_right_side
        stn_outside = STL.SyntaxTreeNode()
        stn_outside.op = 'P'
        stn_outside.interval = STL.Interval(1, 1)
        stn_outside.left_node = stn_inside
        return_list.append(stn_outside.to_formula())

  return return_list


def generate_parametrized_formulas(template_formula, parameter_domains):
    """
    Takes a template formula and parameter domains. Returns parametrized versions of the template formula with all
     parameter combinations within the parameter domain.
    Args:
        template_formula:
        parameter_domains:

    Returns: parametrized formulas

    """

    parameter_formula_list = []
    inf_formula, parameter_domains_for_formula = formula_search.generate_formula_from_template(template_formula, parameter_domains)

    parameter_list = list(parameter_domains_for_formula.keys())
    parameter_domain = [parameter_domains_for_formula[pa] for pa in parameter_list]
    prefix_formula = STL.infix_to_prefix(inf_formula)

    for params in itertools.product(*parameter_domain):
        tmp_formula = prefix_formula
        for p, v in zip(parameter_list, params):
            tmp_formula = tmp_formula.replace(p, str(v))
        parameter_formula_list.append(tmp_formula)
    return parameter_formula_list


def generate_repairable_formulas_for_ta(ta_list, loc_formula_l_count):
  """Generates 3 types of repairable formulas for TA.
      ta_list is the metric list for TA
      loc_formula_l_count is the number of equalities in a phi_l formula, since x0 = 1 | x0 = 1 will be tested during
      parameter optimization of x0 = p0 | x0 = p0 (as well as different assignments) it is sufficient to generate phi_l
      for the given count (not upto). Max count should be location count - 1.
  """

  phi_l_list = []
  """ First generate the location formulas for each TA."""
  for ta_ind  in range(len(ta_list)):
    str_ind = ta_list[ta_ind]
    phi_l_list.append( " | ".join( ['x'+str_ind + ' = ' + 'p' + str_ind  for i in range(loc_formula_l_count[ta_ind])]))


  """For each TA, generate 3 types of templates. """
  guard_less_than_formulas = []
  guard_greater_than_formulas = []
  invariant_formulas = []
  for str_ind in ta_list:
    xi = 'x' + str_ind
    pi = 'p' + str_ind
    base = xi + ' = ' + pi + '_f & A 1 1 ( ! ( ' + xi + ' = ' + pi + '_f ) ) '
    for phi_l in phi_l_list:
      guard_less_than_formulas.append( base + ' & A 1 pAb ( ' + phi_l + ' ) ')
      guard_greater_than_formulas.append( base + ' & P 1 pPb ( ' + phi_l + ' ) ')

    invariant_formulas.append( 'A 0 pAIb ' + xi + ' = ' + pi)

  return guard_less_than_formulas, guard_greater_than_formulas, invariant_formulas
