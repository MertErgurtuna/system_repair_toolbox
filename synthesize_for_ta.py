from constants import stl_constants
from optimization import diagonal_search
from trace_checker import formula_generator
import sys
import time

def synthesize_repairable_formulas_for_ta(trace_count, location_list, location_counts, parameter_bounds, bound, max_d):

  folder_name = 'test_data/ta_repair/'
  signal_file_base = 'sgt'
  #trace_count = 10
  pc = 48 # processor count

  #location_list = ['0'] # maps to metric list. A metric for each TA in NTA
  #location_counts = [4]
  set_valued_metrics = location_list # each of them is set valued.

  # For each TA, parameter domain is the possible location ids.
  # pAb : parameter for guard less than repair
  # pPb : parameter for guard greater than repair
  # pAIb : parameter for invariant repair

  parameter_domains = {'p' + location_list[0] : range(parameter_bounds['p'][0], parameter_bounds['p'][1]),
                       'pAb' : range(parameter_bounds['pAb'][0], parameter_bounds['pAb'][1]),
                       'pPb' : range(parameter_bounds['pPb'][0], parameter_bounds['pPb'][1]),
                       'pAIb' : range(0, 0)}

  glr_formulas, ggr_formulas, inv_formulas = formula_generator.generate_repairable_formulas_for_ta(
    location_list, [1])


  # Fix parameters (ta, location count etc)
  # run diagonal search for glr_formulas + ggr_formulas + inv_formulas

  #bound = 100
  #max_d = 0
  start = time.time()
  diagonal_search.search_template_formulas(glr_formulas + ggr_formulas, folder_name, signal_file_base, trace_count, pc, parameter_domains,  bound, max_d, log_file_name="log", verbose=True)
  end = time.time()
  print(end - start)

trace_count = int(sys.argv[1])
location_list = [sys.argv[2]]
location_counts = [int(sys.argv[3])]
parameter_bounds = {'p':(int(sys.argv[4]), int(sys.argv[5])), 'pAb':(int(sys.argv[6]), int(sys.argv[7])), 'pPb':(int(sys.argv[8]), int(sys.argv[9]))}
bound = int(sys.argv[10])
max_d = int(sys.argv[11])
synthesize_repairable_formulas_for_ta(trace_count, location_list, location_counts, parameter_bounds, bound, max_d)




