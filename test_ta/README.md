Since pyuppaal is implemented in Python2 and our other scripts are implemented in Python3, experiment setup needs both Python2 and Python3. Provided verifyta executable is for 64-bit Linux machines, for other environments replace this executable as needed. Run experiments from the `stl_fs_sm/test_ta/` directory. 

To run experiments for the fischer example:
`python2 test_ta.py fischer`

To run experiments for the db example:
`python2 test_ta.py db`

To run experiments for the sbr example:
`python2 test_ta.py sbr`

To run experiments for the nuclear-plant example:
`python2 test_ta.py nuclear-plant`

To run experiments for the train example:
`python2 test_ta.py train`

To run all experiments for the TA:
`python2 test_ta.py all`


If ./verifyta: Permission denied exception is received while running the examples, please use the following command:
`chmod +x verifyta`

Please refer to the following sources for the original TA models:  

fischer(examples/fischer):  
-Hune, T., Romijn, J., Stoelinga, M., Vaandrager, F.: Linear parametric model checking of timed automata. In: Tools and Algorithms for the Construction and Analysis of Systems, pp. 189–203. Springer Berlin Heidelberg, Berlin, Heidelberg (2001)  
  
db(examples/db):  
-Kölbl, M., Leue, S., Wies, T.: Clock bound repair for timed systems. In: International Conference on Computer Aided Verification, pp. 79–96. Springer (2019)  

sbr(examples/sbr):  
-Kölbl, M., Leue, S., Wies, T.: Tartar: A timed automata repair tool. In: S.K. Lahiri, C. Wang (eds.) Computer Aided Verification, pp. 529–540. Springer International Publishing, Cham (2020)  
-Kölbl, M., Leue, S., Wies, T.: Clock bound repair for timed systems. In: International Conference on Computer Aided Verification, pp. 79–96. Springer (2019)  

nuclear-plant(examples/nuclear-plant):  
-Andre, E., Fribourg, L., Kühne, U., Soulat, R.: Imitator 2.5: A tool for analyzing robustness in scheduling problems. In: FM 2012: Formal Methods, pp. 33–36. Springer Berlin Heidelberg, Berlin, Heidelberg (2012)  
  
train(examples/train):  
-Andre, E., Fribourg, L., Kühne, U., Soulat, R.: Imitator 2.5: A tool for analyzing robustness in scheduling problems. In: FM 2012: Formal Methods, pp. 33–36. Springer Berlin Heidelberg, Berlin, Heidelberg (2012)  
