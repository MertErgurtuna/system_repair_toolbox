import pyuppaal
import subprocess as sp
import sys
import time

new_clock_count = 1
file_name_counter = 1

def mutate_guards_and_test(example_name, checked_location, example, template, list_of_all_locs, ta_list):
    global file_name_counter
    for transition in template.transitions:
        if transition.guard.value:
            atoms = transition.guard.value.split("&&")
            for j in range(len(atoms)):
                left = []
                right = []
                try:
                    left = atoms[0:j]
                except:
                    pass
                try:
                    right = atoms[j + 1:]
                except:
                    pass
                transition.guard.value = "&&".join(left + right)
                f = open("examples/" + example_name + "/" + example_name + str(file_name_counter) + "_modified.xml", "w+")
                f.write(example.to_xml())
                f.close()
                transition.guard.value = "&&".join(atoms)
                if test("examples/" + example_name, example_name + str(file_name_counter) + "_modified", "100", "100", " ".join(list_of_all_locs), checked_location, ta_list, str(len(template.locations)) + " 0 " + str(len(template.locations)) + " 0 10 0 10 0 10 100 0",):
                    verify_repair("examples/" + example_name + "/" + example_name + str(file_name_counter) + "_modified" + "_repaired.xml", "examples/" + example_name + "/" + example_name + ".q")
                    file_name_counter += 1

def mutate_invariants_and_test(example_name, checked_location, example, template, list_of_all_locs, ta_list):
    global file_name_counter
    for location in template.locations:
        if location.invariant.value:
            atoms = location.invariant.value.split("&&")
            for j in range(len(atoms)):
                left = []
                right = []
                try:
                    left = atoms[0:j]
                except:
                    pass
                try:
                    right = atoms[j + 1:]
                except:
                    pass
                location.invariant.value = "&&".join(left + right)
                f = open("examples/" + example_name + "/" + example_name + str(file_name_counter) + "_modified.xml", "w+")
                f.write(example.to_xml())
                f.close()
                location.invariant.value = "&&".join(atoms)
                if test("examples/" + example_name, example_name + str(file_name_counter) + "_modified", "100", "100", " ".join(list_of_all_locs), checked_location, ta_list, str(len(template.locations)) + " 0 " + str(len(template.locations)) + " 0 10 0 10 0 10 100 0"):
                    verify_repair("examples/" + example_name + "/" + example_name + str(file_name_counter) + "_modified" + "_repaired.xml", "examples/" + example_name + "/" + example_name + ".q")
                    file_name_counter += 1

def mutate_test_verify(example_name, checked_location, ta_list):
    global file_name_counter
    file_name_counter = 1
    example = pyuppaal.NTA().from_xml("examples/" + example_name + "/" + example_name + ".xml")
    list_of_all_locs = []
    for template in example.templates:
        list_of_all_locs.extend(map(lambda x: template.name + "." + x.name.value, template.locations))
    for template in example.templates:
        mutate_guards_and_test(example_name, checked_location, example, template, list_of_all_locs, ta_list)
        mutate_invariants_and_test(example_name, checked_location, example, template, list_of_all_locs, ta_list)

def is_a_formula(formula):
    return " A " in formula

def is_p_formula(formula):
    return " P " in formula

def create_transition(transition, guard):
    return pyuppaal.Transition(source=transition.source,
                               target=transition.target,
                               synchronisation=transition.synchronisation.value,
                               select=transition.select.value,
                               assignment=transition.assignment.value,
                               action=transition.action,
                               controllable=transition.controllable,
                               guard=transition.guard.value + guard)

def find_transitions_from_not_timefull_locs(transitions, transition):
    result = []
    if transition.source.urgent or transition.source.committed:
        for t in transitions:
            if transition.source == t.target:
                result += find_transitions_from_not_timefull_locs(transitions, t)
    else:
        result += [transition]
    return result

    for transition in transitions:
        if transition_for_guards.source == transition.target:
            return_transition
            if transition.source.urgent or transition.source.committed:
                temp = find_transitions_from_not_timefull_locs(transitions, transition)

def is_timefull_loc(location_name, example):
    timefull_transitions = []
    clocks_zero_in_location = []
    template_found = False
    for template in example.templates:
        transitions = filter(lambda x: x.source.name.value == location_name, template.transitions)
        if len(transitions) > 0:
            template_found = True
        for transition in template.transitions:
            if transition.target.name.value == location_name:
                clocks_zero_in_location.extend(map(lambda x: x.strip().split("=")[0].strip(), transition.assignment.value.replace("\n", "").replace(":", "").split(",")))
        timefull_transitions = map(lambda x: False, transitions)
        last_index = 0
        for transition in transitions:
            atoms = map(lambda x: x.strip().replace(">=", ">").replace("<=", "<"), transition.guard.value.split("&&"))
            for atom in atoms:
                if ">=" in atom:
                    left, right = map(lambda x: x.strip(), atom.split(">="))
                    for clock in clocks_zero_in_location:
                        if left == clock and right != "0":
                            timefull_transitions[last_index] = True
                            last_index += 1
                elif ">" in atom:
                    left, right = map(lambda x: x.strip(), atom.split(">"))
                    for clock in clocks_zero_in_location:
                        if left == clock:
                            timefull_transitions[last_index] = True
                            last_index += 1
                if "<=" in atom:
                    left, right = map(lambda x: x.strip(), atom.split("<="))
                    for clock in clocks_zero_in_location:
                        if left != "0" and right == clock:
                            timefull_transitions[last_index] = True
                            last_index += 1
                elif "<" in atom:
                    left, right = map(lambda x: x.strip(), atom.split("<"))
                    for clock in clocks_zero_in_location:
                        if right == clock:
                            timefull_transitions[last_index] = True
                            last_index += 1
        if template_found:
            break
    if timefull_transitions == []:
        return False
    return reduce(lambda x, y: x and y, timefull_transitions)

def repair_ta_for_a_formula(formula, c1, c2, example, ta):
    formula = formula.split(" & ")
    li = formula[0].split(" = ")[1].split()[0]
    lj = formula[2].split(" = ")[1].split()[0]
    b = formula[2].split()[2]
    li_name = get_location_name(li, ta)
    lj_name = get_location_name(lj, ta)
    is_lj_init_loc = True if lj_name in map(lambda x: x.initlocation.name.value, example.templates) else False
    is_lj_timefull_loc = is_timefull_loc(lj_name, example)
    is_declared = False
    for template in example.templates:
        new_transitions = []
        for transition in template.transitions:
            if transition.target.name.value == li_name:
                if not is_declared:
                    template.declaration += "\nclock " + c1 + ";\nclock " + c2 + ";\n"
                    is_declared = True;
                transitions_for_guards = find_transitions_from_not_timefull_locs(template.transitions, transition)
                for transition_for_guards in transitions_for_guards:
                    if transition_for_guards.guard.value != "":
                        transition_for_guards.guard.value += " && "
                    new_transitions.append(create_transition(transition_for_guards, c1 + " > " + c2))
                    if not is_lj_init_loc and is_lj_timefull_loc:
                        new_transitions.append(create_transition(transition_for_guards, c1 + " == " + c2))
                    transition_for_guards.guard.value += c1 + " < " + c2 + " && "+ c1 + " < " + b
        template.transitions.extend(new_transitions)
        for transition in template.transitions:
            if transition.target.name.value == lj_name:
                if transition.assignment.value != "":
                    transition.assignment.value += ", "
                transition.assignment.value += c1 + " = 0"
            if transition.source.name.value == lj_name:
                if transition.assignment.value != "":
                    transition.assignment.value += ", "
                transition.assignment.value += c2 + " = 0"

def repair_ta_for_p_formula(formula, c1, c2, example, ta):
    formula = formula.split(" & ")
    li = formula[0].split(" = ")[1].split()[0]
    lj = formula[2].split(" = ")[1].split()[0]
    b = formula[2].split()[2]
    li_name = get_location_name(li, ta)
    lj_name = get_location_name(lj, ta)
    is_lj_init_loc = True if lj_name in map(lambda x: x.initlocation.name.value, example.templates) else False
    is_lj_timefull_loc = is_timefull_loc(lj_name, example)
    is_declared = False
    for template in example.templates:
        new_transitions = []
        for transition in template.transitions:
            if transition.target.name.value == li_name:
                if not is_declared:
                    template.declaration += "\nclock " + c1 + ";\nclock " + c2 + ";\n"
                    is_declared = True
                if transition.guard.value != "":
                    transition.guard.value += " && "
                if not is_lj_init_loc and is_lj_timefull_loc:
                    new_transitions.append(create_transition(transition, c1 + " == " + c2))
                transition.guard.value += c1 + " > " + c2 + " && "+ c2 + " > " + b
        template.transitions.extend(new_transitions)
        for transition in template.transitions:
            if transition.target.name.value == lj_name:
                if transition.assignment.value != "":
                    transition.assignment.value += ", "
                transition.assignment.value += c1 + " = 0"
            if transition.source.name.value == lj_name:
                if transition.assignment.value != "":
                    transition.assignment.value += ", "
                transition.assignment.value += c2 + " = 0"

def get_location_name(l, ta):
    templates = sp.check_output("""cat ../test_data/ta_repair/options.txt""", shell=True).split(" | ")
    for temp in templates:
        if temp[:temp.find(":")] == ta:
            return temp.split("{")[1].split("}")[0].split(": " + l)[0].split(", ")[-1]

def repair_ta(path, example_name, formulae, ta):
    global new_clock_count
    new_clock_count = 1
    example = pyuppaal.NTA().from_xml(path + "/" + example_name + ".xml")
    formulae = formulae.split(" | ")
    for formula in formulae:
        c1 = "c" + str(new_clock_count)
        new_clock_count += 1
        c2 = "c" + str(new_clock_count)
        new_clock_count += 1
        if is_p_formula(formula):
            repair_ta_for_p_formula(formula, c1, c2, example, ta)
        elif is_a_formula(formula):
            repair_ta_for_a_formula(formula, c1, c2, example, ta)
        else:
            print "Error"
    f = open(path + "/" + example_name + ".xml", "w+")
    f.write(example.to_xml())
    f.close()
    print "Model repaired and saved to " + path + "/" + example_name + ".xml"

def generate_traces(path, example_name, traces, bound, locations, is_faulty):
    output_tracer = sp.check_output("cp " + path + "/" + example_name + ".xml ./; python3 tracer.py " + example_name + ".xml " + traces + " " + bound + " -l " + locations + " -d -f " + is_faulty +  " -d -x; rm " + example_name + ".xml", shell=True)
    percentage = float(output_tracer.split()[-1][:-1])
    if percentage > 0:
        print output_tracer[:-1]
    return percentage

def synthesize_formula_and_repair(path, example_name, traces, config, ta):
    start = time.time()
    output_test = sp.check_output("cd ../; python3 synthesize_for_ta.py " + traces + " " + config + "; cd -", shell=True)
    output_test = output_test.split("\n")
    result, synthesis_time = output_test[0].split(" TP"), output_test[1]
    formulae, stats = result[0], "TP" + result[1]
    repair_ta(path, example_name, formulae, ta)
    end = time.time()
    print "Synthesized formulae:", formulae
    print "Synthesis stats:", stats
    print "Synthesis time:", synthesis_time
    print "Total of synthesis and repair time:", end - start

def verify_repair(xml_file, q_file):
    output_verify = sp.check_output("./verifyta " + xml_file + " " + q_file + " 2>&-", shell=True).strip()
    print "Verification result after repair:", output_verify.split("\n")[-1].strip().split("--")[-1].strip() + "\n"

def test(path, example_name, traces, bound, locations, is_faulty, ta_list, config):
    is_faulty_percentage = generate_traces(path, example_name, traces, bound, locations, is_faulty)
    if is_faulty_percentage > 0:
        sp.call("cp " + path + "/" + example_name + ".xml " + path + "/" + example_name + "_repaired.xml", shell=True)
        for i in range(len(ta_list)):
            synthesize_formula_and_repair(path, example_name + "_repaired" , traces, str(i) + " " + config, ta_list[i])
        return True
    return False

def fischer():
    test("examples/fischer", # Path to file
             "fischer", # Example name
             "100", # Number of traces
             "100", # Duration
             "Process1.cs Process1.start Process1.set Process1.try_enter Process2.cs", # Locations
             "'Process1.cs and Process2.cs'", # isFaulty
             ["Process1"], # TA list
             "4 0 2 0 6 0 6 200 0" # Configuration for synthesize_for_ta.py
             )
    verify_repair("examples/fischer/fischer_repaired.xml", "examples/fischer/fischer.q")

def db():
    test("examples/db", # Path to file
             "db", # Example name
             "100", # Number of traces
             "100", # Duration
             "dbServer.reqSent dbServer.error dbServer.initial dbServer.serReceiving dbServer.reqCreate dbServer.timeout", # Locations
             "'dbServer.error'", # isFaulty
             ["dbServer"], # TA list
             "6 0 6 0 4 0 4 100 0" # Configuration for synthesize_for_ta.py
             )
    verify_repair("examples/db/db_repaired.xml", "examples/db/db.q")

def sbr():
    test("examples/sbr", # Path to file
             "sbr", # Example name
             "100", # Number of traces
             "100", # Duration
             "Cyclic50.cyclic50_run Cyclic50.cyclic50_idle Cyclic150.cyclic150_run Cyclic150.cyclic150_idle Cyclic200.cyclic200_run Cyclic200.cyclic200_idle Processor.error1 Processor.error2 Processor.error3", # Locations
             "'Processor.error1 or Processor.error2 or Processor.error3'", # isFaulty
             ["Cyclic50", "Cyclic150", "Cyclic200"], # TA list
             "2 0 2 2 10 0 0 200 0" # Configuration for synthesize_for_ta.py
             )
    verify_repair("examples/sbr/sbr_repaired.xml", "examples/sbr/sbr.q")

def nuclear_plant():
    mutate_test_verify("nuclear-plant", "plant.boom", ["plant"])

def train():
    mutate_test_verify("train", "machine.crash", ["machine"])

def main():
    if sys.argv[1] == "fischer":
        fischer()
    elif sys.argv[1] == "db":
        db()
    elif sys.argv[1] == "sbr":
        sbr()
    elif sys.argv[1] == "nuclear-plant":
        nuclear_plant()
    elif sys.argv[1] == "train":
        train()
    elif sys.argv[1] == "all":
        fischer()
        db()
        sbr()
        nuclear_plant()
        train()

main()
