import subprocess as sp
import argparse
import errno
import sys
import os

def init():
    global filename, N, bound, locations, ints, clocks, step, seed, delete, formula, all_traces, locations_map, count, resolution, xx, yy, zz
    parser = argparse.ArgumentParser(description="""This is a random timed automata 
                                        trace generator. It is users responsible to 
                                        provide a valid XML file that can be read by 
                                        UPPAAL. This XML file shall not contain any 
                                        query. The program creates a query file with 
                                        the inferred expression and "traces" folder
                                        with generated random traces.""")
    parser.add_argument("filename", type=str, help="name of the xml readable by uppaal")
    parser.add_argument("traces", type=int, help="number of traces generated")
    parser.add_argument("bound", type=int, help="time bound for generated traces")
    parser.add_argument("-l", "--locations", nargs="+", type=str, help="""sets 
                        location state variables""")
    parser.add_argument("-i", "--ints", nargs="+", type=str, help="""sets discrete 
                        state variables (integer and boolean variables)""")
    parser.add_argument("-c", "--clocks", nargs="+", type=str, help="""sets clock 
                        state variables (only clock variables)""")
    parser.add_argument("-s", "--step", type=float, help="""sets time step for 
                        trace generation (default is 1)""")
    parser.add_argument("-r", "--resolution", type=float, help="""sets resolution for 
                        trace generation (default is 1)""")
    parser.add_argument("-d", "--delete", action="store_true", help="""deletes 
                        to-be-created files if they already exist""")
    parser.add_argument("-f", "--formula", nargs="?", type=str, help="""indicates logical
                        expression for formula checking (give expression in quotion marks) 
                        atoms checked in the proposition must be given for either locations, 
                        integers, or clocks""")
    parser.add_argument("-x", "--xx", action="store_true", help="""labels only on change""")
    parser.add_argument("-y", "--yy", action="store_true", help="""writes labeled traces only""")
    parser.add_argument("-z", "--zz", action="store_true", help="""stops after first positive label""")
    args = parser.parse_args()
    if not (args.locations or args.ints or args.clocks):
        print("At least one of --location, --int, or --clock must be given.")
        sys.exit()
    filename = args.filename
    N = args.traces
    bound = args.bound
    locations = sorted(args.locations) if args.locations else []
    ints = sorted(args.ints) if args.ints else []
    clocks = sorted(args.clocks) if args.clocks else []
    step = args.step if args.step else 1.0
    resolution = args.resolution if args.resolution else 1.0
    delete = args.delete
    xx = args.xx
    yy = args.yy
    zz = args.zz
    formula = args.formula.replace("(", "[").replace(")", "]") if args.formula else []
    locations_map = {}
    counters = {}
    if len(locations) == 1:
        locations = locations[0].split(" ")
    if len(ints) == 1:
        ints = ints[0].split(" ")
    if len(clocks) == 1:
        clocks = clocks[0].split(" ")
    query = ("simulate " + str(N) + " [<=" + str(bound) + "] {"
                                    + ", ".join(locations + ints + clocks) + "}")
    query_filename = filename.split(".xml")[0] + ".q"
    with open(query_filename, "w+" if delete else "x") as f:
        f.write(query)
    locations = [i.replace("(", "[").replace(")", "]") for i in locations]
    ints = [i.replace("(", "[").replace(")", "]") for i in ints]
    clocks = [i.replace("(", "[").replace(")", "]") for i in clocks]
    if locations:
        for l in locations:
            temp, loc = l.split(".")
            if  temp not in locations_map.keys():
                locations_map[temp] = {}
                counters[temp] = 0
            locations_map[temp][loc] = counters[temp]
            counters[temp] += 1

def generate_traces(trace_goal):
    global filename, N, bound, locations, ints, clocks, step, seed, delete, formula, all_traces, locations_map, count, resolution, xx, yy, zz
    try:
        output = sp.check_output("./verifyta -q " + seed + filename + " " + filename.split(".xml")[0] + ".q",
                    shell=True).decode().split(" -- Formula is satisfied.")[1].split("\n")[1:-1]
    except:
        return False
    all_traces = {}
    last_state_var = ""
    temp = trace_goal
    for i in output:
        try:
            trace_num, trace = i.split(":")
            trace_num = int(trace_num.strip()[1:-1])
            trace = list(map(lambda x: tuple(map(lambda y: float(y) if "." in y else int(y),
                                                x[1:-1].split(","))), trace.strip().split()))
            all_traces[last_state_var][trace_num] = trace
            if trace == [] and temp > trace_num:
                    temp = trace_num
        except:
            last_state_var = i.split(":")[0].strip()
            all_traces[last_state_var] = {}
    if temp != trace_goal:
        N = temp
    return True

def write_traces(trace_count):
    global filename, N, bound, locations, ints, clocks, step, seed, delete, formula, all_traces, locations_map, count, resolution, xx, yy, zz
    try:
        os.mkdir("traces")
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
    with open("../test_data/ta_repair/options.txt", "w+" if delete else "x") as f:
            f.write(" | ".join([temp + ": " + str(locations_map[temp]).replace("'", "") for temp in
                                                        locations_map.keys()] + ints + clocks) + "\n")
    precision_required = len(str(step).split(".")[1])
    labeled_counter = 0
    for trace in range(N):
        time = 0
        trace_file_content = ""
        prev_label = "0"
        labeled = False
        if formula:
            formula_file_content = ""
        while (time <= bound):
            line = str(int(round(time/step, precision_required)*resolution))
            if formula:
                expression = formula
            for temp in locations_map.keys():
                found = False
                writtten = False
                for loc in locations_map[temp]:
                    value = 0
                    for state in all_traces[temp + "." + loc][trace]:
                        if time >= state[0]:
                            value = state[1]
                        else:
                            if value != 0:
                                line += " " + str(locations_map[temp][loc])
                                writtten = True
                                found = True
                            if formula:
                                expression = expression.replace(temp + "." + loc, str(value))
                            break
                    if value != 0 and not writtten:
                        line += " " + str(locations_map[temp][loc])
                        writtten = True
                        found = True
                    if formula:
                        expression = expression.replace(temp + "." + loc, str(value))
                if not found:
                    line += " -1"
                    if formula:
                        expression = expression.replace(temp + "." + loc, str(value))
            for state_var in ints:
                value = 0
                for state in all_traces[state_var][trace]:
                    if time >= state[0]:
                        value = state[1]
                    else:
                        line += " " + str(value)
                        if formula:
                            expression = expression.replace(state_var, str(value))
                        break
            for state_var in clocks:
                value = 0
                s = None
                for state in all_traces[state_var][trace]:
                    if time >= state[0]:
                        value = time - (state[0] - state[1])
                        s = state
                    else:
                        break
                value = round(value, precision_required)
                line += " " + str(int(value*resolution))
                if formula:
                    expression = expression.replace(state_var, str(value))
            trace_file_content += line + "\n"
            if formula:
                formula_file_content += str(int(round(time/step, precision_required)*resolution)) + " "
                label = ("1" if eval(expression.replace("[", "(").replace("]", ")")) else "0")
                if label == "1":
                    labeled = True
                    if xx:
                        if prev_label == "0":
                            formula_file_content += ("1" + "\n")
                            count += 1
                            prev_label = "1"
                        else:
                            formula_file_content += ("0" + "\n")
                    else:
                            formula_file_content += ("1" + "\n")
                            count += 1
                            prev_label = "1"
                    if zz:
                        break
                else:
                    formula_file_content += ("0" + "\n")
                    prev_label = "0"
            time += step
        if yy:
            if labeled:
                with open("../test_data/ta_repair/sgt_" + str(labeled_counter + trace_count), "w+" if delete else "x") as f:
                    f.write(trace_file_content)
                if formula:
                    with open("../test_data/ta_repair/sgt_" + str(labeled_counter + trace_count) + "_label", "w+" if delete else "x") as f:
                        f.write(formula_file_content)
                labeled_counter += 1
        else:
            with open("../test_data/ta_repair/sgt_" + str(trace + trace_count), "w+" if delete else "x") as f:
                f.write(trace_file_content)
            if formula:
                with open("../test_data/ta_repair/sgt_" + str(trace + trace_count) + "_label", "w+" if delete else "x") as f:
                    f.write(formula_file_content)

def main():
    global filename, N, bound, locations, ints, clocks, step, seed, delete, formula, all_traces, locations_map, count, resolution, xx, yy, zz
    init()
    trace_goal = N
    trace_count = 0
    r = 16
    count = 0
    while trace_count < trace_goal:
        seed = "-r " + str(r) + " "
        if generate_traces(trace_goal):
            if N != 0:
                write_traces(trace_count)
                trace_count += N
        r += 1
        if formula:
            try:
                temp = str(round(count/((trace_count*bound)/step)*100, 2))
            except:
                temp = "0"
            print("For " + str(trace_count) + " traces, formula satisfaction rate is " + temp + "%", end="\r", flush=True)
        else:
            print(str(trace_count) + " traces generated", end="\r", flush=True)
    print()
    sp.call("rm " + filename.split(".xml")[0] + ".q", shell=True)

if __name__ == '__main__':
    main()

