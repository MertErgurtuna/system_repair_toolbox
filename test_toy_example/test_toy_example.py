import subprocess as sp
import sys

def test_toy_example():
    output_test = sp.check_output("""cd ../; python3 synthesize_for_toy_ex.py; cd -""", shell=True).split("\n")
    formulas = output_test[0].split(" TP: ")[0].split("|")
    print (formulas)


if __name__ == '__main__':
    test_toy_example()


