from trace_checker import STL
from operator import itemgetter
from optimization import evaluator
from multiprocessing import Pool
from functools import partial
from constants import stl_constants
from optimization import evaluation_result
from optimization import grid_search
import itertools
import time


def evaluate_signals_from_param_diagonal(params, parameter_list, params_to_optimize,
                                         parameter_ranges_to_optimize, parameter_mon_to_optimize,
                                         org_parameter_list, formula, folder_name, signal_file_base, trace_count,
                                         signal_file_rest, return_type):
    parameter_ranges_optimized = modify_parameter_ranges(parameter_ranges_to_optimize, params_to_optimize,
                                                         parameter_list, params)
    tmp_formula = formula
    for p, v in zip(parameter_list, params):
        tmp_formula = tmp_formula.replace(p, str(v))

    best_eval, best_indexes, detailed_result, param_ret = search(tmp_formula, params_to_optimize,
                                                                 parameter_ranges_optimized,
                                                                 parameter_mon_to_optimize,
                                                                 folder_name, signal_file_base,
                                                                 trace_count, signal_file_rest,
                                                                 return_type)
    received_params = list(params)
    param_index = 0
    for param in org_parameter_list:
        if param not in parameter_list:
            if param == param_ret[0]:
                received_params.insert(param_index, best_indexes[0])
            else:
                received_params.insert(param_index, best_indexes[1])
        param_index = param_index + 1
    return detailed_result, tuple(received_params)




def stn_checker(stn, list):
    if stn == []:
        pass
    elif stn.op == '&' or stn.op == '|':
        stn_checker(stn.left_node, list)
        stn_checker(stn.right_node, list)
    elif stn.op == '>' or stn.op == '<':
        list.append([stn.param, stn.op, ''])
    elif stn.op == 'A' or stn.op == 'P':
        list.append([stn.interval.ub, stn.op, ''])
        if stn.left_node.op == '>' or stn.left_node.op == '<':
            list.append([stn.left_node.param, stn.left_node.op, stn.op])
        elif stn.left_node.op == 'A' or stn.left_node.op == 'P':
            stn_checker(stn.left_node, list)
        elif stn.left_node.op == '&' or stn.left_node.op == '|':
            stn_checker(stn.left_node.left_node, list)
            stn_checker(stn.left_node.right_node, list)
    return list


def get_diagonal_parameters(formula, parameter_list, parameter_domain):
    index = 0
    indexes = []
    is_found = False
    tmp_formula = formula
    for parameter in parameter_list:
        while tmp_formula.find(str(index)) != -1:
            index = index + 1
        indexes.append(index)
        tmp_formula = tmp_formula.replace(parameter, str(index))
        index = index + 1
    stn = STL.SyntaxTreeNode()
    stn.initialize_node(tmp_formula.split(), 0)
    stn_l = []
    parameter_space = []
    if formula.find('!') == -1:
        parameter_space = stn_checker(stn, stn_l)
    parameter_list_mon = []
    parameter_domain_mon = []
    parameter_monotoniciy = []
    for parameter in parameter_space:
        if parameter[0] in indexes:
            index_in_list = indexes.index(int(parameter[0]))
            parameter_list_mon.append(parameter_list[index_in_list])
            parameter_domain_mon.append(parameter_domain[index_in_list])
            parameter_monotoniciy.append([parameter[1], parameter[2]])
    if len(parameter_list_mon) > 1:
        len_of_list = [len(x) for x in parameter_list_mon]
        parameter_list_sorted = [x for _, x in
                                 sorted(zip(len_of_list, parameter_list_mon), key=itemgetter(0), reverse=True)]
        parameter_domain_sorted = [x for _, x in
                                   sorted(zip(len_of_list, parameter_domain_mon), key=itemgetter(0), reverse=True)]
        parameter_monotonicity_sorted = [x for _, x in
                                         sorted(zip(len_of_list, parameter_monotoniciy), key=itemgetter(0),
                                                reverse=True)]
        parameter_list_sorted = parameter_list_sorted[0:2]
        parameter_domain_sorted = parameter_domain_sorted[0:2]
        parameter_monotonicity_sorted = parameter_monotonicity_sorted[0:2]
        is_found = True
    else:
        parameter_list_sorted = []
        parameter_domain_sorted = []
        parameter_monotonicity_sorted = []
    return parameter_list_sorted, parameter_domain_sorted, parameter_monotonicity_sorted, is_found


def modify_parameter_ranges(parameter_ranges_to_optimize, params_to_optimize, parameter_list, params):
    parameter_ranges_to_optimize_cpy = parameter_ranges_to_optimize[:]
    index_param = 0
    for parameter in parameter_list:
        if parameter.startswith('pA'):
            paramNumber = int(parameter.replace('pA', ''))
            paramMinVal = params[index_param]
            if paramNumber % 2 == 0:
                index = 0
                for param in params_to_optimize:
                    if 'pA' + str(paramNumber + 1) == param:
                        domain = parameter_ranges_to_optimize_cpy[index]
                        if len(domain) > 1:
                            step_size = domain[1] - domain[0]
                            max_of_range = max(domain) + step_size
                            new_range = range(paramMinVal, max_of_range, step_size)
                        else:
                            new_range = range(paramMinVal, paramMinVal, 1)
                        parameter_ranges_to_optimize_cpy[index] = new_range
                    index = index + 1
        index_param = index_param + 1

    index_param = 0
    parameter_ranges_to_optimize_cpy_p = parameter_ranges_to_optimize[:]
    for parameter in parameter_list:
        if parameter.startswith('pP'):
            paramNumber = int(parameter.replace('pP', ''))
            paramMinVal = params[index_param]
            if paramNumber % 2 == 0:
                index = 0
                for param in params_to_optimize:
                    if 'pP' + str(paramNumber + 1) == param:
                        domain = parameter_ranges_to_optimize_cpy_p[index]
                        if len(domain) > 1:
                            step_size = domain[1] - domain[0]
                            max_of_range = max(domain) + step_size
                            new_range = range(paramMinVal, max_of_range, step_size)
                        else:
                            new_range = range(paramMinVal, paramMinVal, 1)
                        parameter_ranges_to_optimize_cpy[index] = new_range
                    index = index + 1
        index_param = index_param + 1
    return parameter_ranges_to_optimize_cpy


def evaluate_signals_with_criteria(formula, folder_name, signal_file_base, trace_count, signal_file_rest,
                                   return_type):
    evaluation = evaluator.evaluate_signals(formula, folder_name, signal_file_base, trace_count, signal_file_rest,
                                            stn=None, past_results=[])

    if return_type.name == 'fn':  # optimal_criteria == stl_constants.CATEGORY_FALSE_NEGATIVE:
        return evaluation.fn, evaluation.tn, evaluation # if breaks, evaluation.get_result(stl_constants.OptimizationObjective('detailed'))
    if return_type.name == 'fp':  # optimal_criteria == stl_constants.CATEGORY_FALSE_POSITIVE:
        return evaluation.fp, evaluation.tp, evaluation
    if return_type.name == 'tn':  # optimal_criteria == stl_constants.CATEGORY_TRUE_NEGATIVE:
        return evaluation.tn, evaluation.fn, evaluation
    if return_type.name == 'tp':  # optimal_criteria == stl_constants.CATEGORY_TRUE_POSITIVE:
        return evaluation.tp, evaluation.fp, evaluation


def get_optimization_result(parameter_list_org, parameter_domain, formula, folder_name,
                            signal_file_base, trace_count, return_type, process_count,
                            log_file=None, verbose=False):
    start_time = time.time()
    parameter_list = parameter_list_org[:]
    params, ranges, monotonicities, optimization = get_diagonal_parameters(formula, parameter_list, parameter_domain)
    if optimization:
        for index in range(2):
            parameter_domain.pop(parameter_list.index(params[index]))
            parameter_list.pop(parameter_list.index(params[index]))
        if len(parameter_domain) > 0:
            partial_evaluate_signals = partial(evaluate_signals_from_param_diagonal,
                                               parameter_list=parameter_list,
                                               params_to_optimize=params,
                                               parameter_ranges_to_optimize=ranges,
                                               parameter_mon_to_optimize=monotonicities,
                                               org_parameter_list=parameter_list_org,
                                               formula=formula,
                                               folder_name=folder_name,
                                               signal_file_base=signal_file_base,
                                               trace_count=trace_count,
                                               signal_file_rest='',
                                               return_type=return_type)
            pool = Pool(processes=process_count)
            results = pool.map(partial_evaluate_signals, itertools.product(*parameter_domain))
            pool.close()
            pool.join()
        else:
            best_eval, best_indexes, detailed_result, param_ret = search(formula, params,
                                                                         ranges,
                                                                         monotonicities,
                                                                         folder_name,
                                                                         signal_file_base, trace_count,
                                                                         '', return_type)

            if parameter_list_org[0] == params[0]:
                ranges = (str(best_indexes[0]), str(best_indexes[1]))
            else:
                ranges = (str(best_indexes[1]), str(best_indexes[0]))
            results = [[detailed_result, ranges]]
        if log_file and verbose:
            for r in results:
                log_file.write("Parameters " + " ".join([str(s) for s in r[1]]) +
                               " valuation " + str(r[0]) + "\n")
        best_result = evaluation_result.get_best_result(results, return_type)
        if best_result:
            best_val = best_result[0].get_result(return_type)
            best_params = best_result[1]
    else:
        best_val, best_params, _ = grid_search.grid_search(formula=formula, parameter_list=parameter_list,
                                                           parameter_domain=parameter_domain,
                                                           folder_name=folder_name,
                                                           signal_file_base=signal_file_base,
                                                           trace_count=trace_count,
                                                           signal_file_rest='', process_count=process_count,
                                                           return_type=return_type,
                                                           past_results=[],
                                                           log_file=log_file,
                                                           verbose=verbose)


    end_time = time.time()
    return best_val, best_params, end_time-start_time


def return_monotonicity(firstOp, monotonicity):
    if firstOp == '<' and monotonicity.name == 'tn':  # stl_constants.CATEGORY_TRUE_NEGATIVE:
        return stl_constants.MONOTONICALLY_DECREASING
    if firstOp == '>' and monotonicity.name == 'tn':  # stl_constants.CATEGORY_TRUE_NEGATIVE:
        return stl_constants.MONOTONICALLY_INCREASING
    if firstOp == 'A' and monotonicity.name == 'tn':  # stl_constants.CATEGORY_TRUE_NEGATIVE:
        return stl_constants.MONOTONICALLY_INCREASING
    if firstOp == 'P' and monotonicity.name == 'tn':  # stl_constants.CATEGORY_TRUE_NEGATIVE:
        return stl_constants.MONOTONICALLY_DECREASING
    if firstOp == '<' and monotonicity.name == 'tp':  # stl_constants.CATEGORY_TRUE_POSITIVE:
        return stl_constants.MONOTONICALLY_INCREASING
    if firstOp == '>' and monotonicity.name == 'tp':  # stl_constants.CATEGORY_TRUE_POSITIVE:
        return stl_constants.MONOTONICALLY_DECREASING
    if firstOp == 'A' and monotonicity.name == 'tp':  # stl_constants.CATEGORY_TRUE_POSITIVE:
        return stl_constants.MONOTONICALLY_DECREASING
    if firstOp == 'P' and monotonicity.name == 'tp':  # stl_constants.CATEGORY_TRUE_POSITIVE:
        return stl_constants.MONOTONICALLY_INCREASING


def return_matches(formula, p1, p1_range, monotonicity_1, p2, p2_range, monotonicity_2, return_type,
                   folder_name, signal_file_base, trace_count, signal_file_rest):
    number_of_eval = 0
    best_optimal = 0
    detailed_result = evaluation_result.EvaluationResult()
    is_best_found = False
    p1_array = []
    p2_array = []
    best_index_p1 = -1
    best_index_p2 = -1
    if monotonicity_1 == stl_constants.MONOTONICALLY_DECREASING:
        for val in reversed(p1_range):
            p1_array.append(val)
    else:
        for val in p1_range:
            p1_array.append(val)
    if monotonicity_2 == stl_constants.MONOTONICALLY_DECREASING:
        for val in reversed(p2_range):
            p2_array.append(val)
    else:
        for val in p2_range:
            p2_array.append(val)
    index_p1 = 0
    index_p2 = len(p2_array) - 1

    while index_p1 < len(p1_array) and (index_p2 >= 0):
        tmp_formula = formula.replace(p1, str(p1_array[index_p1]))
        tmp_formula = tmp_formula.replace(p2, str(p2_array[index_p2]))
        optimal, complementary, detailed = evaluate_signals_with_criteria(formula=tmp_formula, folder_name=folder_name,
                                                                          signal_file_base=signal_file_base,
                                                                          trace_count=trace_count,
                                                                          signal_file_rest=signal_file_rest,
                                                                          return_type=return_type)
        number_of_eval += 1
        if complementary <= return_type.bound:
            if best_optimal < optimal:
                best_optimal = optimal
                detailed_result = detailed
                best_index_p1 = index_p1
                best_index_p2 = index_p2
                is_best_found = True
            index_p1 = index_p1 + 1
        else:
            index_p2 = index_p2 - 1
    if is_best_found:
        return best_optimal, [p1_array[best_index_p1], p2_array[best_index_p2]], detailed_result, [p1, p2]
    else:
        return 0, [-1, -1], evaluation_result.EvaluationResult(), [p1, p2]


def search(formula, parameter_list, parameter_domain, parameter_monotonicites, folder_name, signal_file_base,
           trace_count, signal_file_rest, return_type):
    p1, p1_range = parameter_list[0], parameter_domain[0]
    p2, p2_range = parameter_list[1], parameter_domain[1]
    monotonicity_1 = return_monotonicity(parameter_monotonicites[0][0], return_type)
    monotonicity_2 = return_monotonicity(parameter_monotonicites[1][0], return_type)
    best_eval, best_indexes, detailed_result, parameters = return_matches(formula, p1, p1_range, monotonicity_1, p2,
                                                                          p2_range,
                                                                          monotonicity_2,
                                                                          return_type,
                                                                          folder_name,
                                                                          signal_file_base,
                                                                          trace_count,
                                                                          signal_file_rest)
    return best_eval, best_indexes, detailed_result, parameters
