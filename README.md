This tool implements the algorithms presented in:  
"An Automated System Repair Framework with Signal Temporal Logic", M. Ergurtuna, B. Yalcinkaya, E. Aydin Gol.

Please follow the guidelines for each example:  
  
Toy example:  
Change directory to test_toy_example and follow instructions in README.md  
Traffic network:  
Change directory to test_dyn_sys and follow instructions in README.md  
fischer example:  
Change directory to test_ta and follow instructions in README.md  
db example:  
Change directory to te st_ta and follow instructions in README.md  
sbr example:  
Change directory to test_ta and follow instructions in README.md  
nuclear-plant example:  
Change directory to test_ta and follow instructions in README.md  
train example:  
Change directory to test_ta and follow instructions in README.md  
