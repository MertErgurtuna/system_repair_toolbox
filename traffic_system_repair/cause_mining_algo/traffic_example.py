
from trace_checker import STL
from cause_mining_algo import helper_funs
from signal_generation import sg
import cause_mining_algo as cma
import numpy as np

import network
import read_input

def controller_traffic_data(viol_formula, folder_name, trace_count, duration, pc, cause_formula):
    """

    Args:
        cause_formula: formula got from cause_mining_traffic_data (in prefix form)

    Returns:

    """
    traffic_file_no_soln = "test_data/traffic_data/l6_system"
    traffic_file = "test_data/traffic_data/l5_system"
    link_dict, intersection_dict = read_input.load_from_annotated_file(traffic_file_no_soln)
    tn = network.Network(link_dict, intersection_dict, scale=5)

    uk_count = tn.get_intersection_count()
    xk_initialized = np.zeros(tn.get_link_count())
    tn.initialize_links(xk_initialized, 0.1)

    viol_formula_prefix = STL.infix_to_prefix(viol_formula)
    sg.controller(folder_name=folder_name, name='test', trace_count=trace_count,  duration=duration, viol_formula=viol_formula_prefix,
                  cause_formula=cause_formula, step_function=tn.step, xk_initialized=xk_initialized, uk_domain=[0, 1],
                  uk_count=uk_count, num_to_let=True)



def traffic_example_main(only_x1):
    test_folder = 'test_data/traffic_data/test2/'
    viol_formula_x1 = "x1 < 30"
    viol_formula_all = " ( x0 < 30 ) & ( x1 < 30 )  & ( x2 < 30 )  & ( x3 < 15 )  & ( x4 < 15 ) & ( x5 < 15 )  "

    duration = 100
    pc = 24
    cause_formulas_x1 = [
        ' ( ( A 1 2 ( x7 = 1 ) ) & ( P 1 1 ( x1 > 23 ) ) ) ',
        ' ( ( A 1 2 ( x7 = 1 ) ) & ( P 1 1 ( ( x1 > 15 ) & ( x6 = 0 ) ) ) )'
    ]


    cause_formulas_all = [
        ' ( ( A 1 2 ( x6 = 0 ) ) & ( P 1 1 ( x6 = 0 ) ) )  ',
        ' ( ( A 1 2 ( x6 = 1 ) ) & ( P 1 1 ( x6 = 1 ) ) )  ',
        ' ( ( A 1 2 ( x7 = 0 ) ) & ( P 1 1 ( x7 = 0 ) ) )  ',
        ' ( ( A 1 2 ( x7 = 1 ) ) & ( P 1 1 ( x7 = 1 ) ) )  '
    ]


    if only_x1:
        viol_formula = viol_formula_x1
        cause_formulas= cause_formulas_x1
    else:
        viol_formula = viol_formula_all
        cause_formulas= cause_formulas_all

    cause_formula = helper_funs.concat_with_or_infix(cause_formulas)
    cause_formula = STL.infix_to_prefix(cause_formula)
    folder_name = test_folder
    controller_traffic_data(folder_name=folder_name, viol_formula=viol_formula, trace_count=20,
                            duration=duration, pc=pc, cause_formula=cause_formula)

    print "Final violation count:  " + str(cma.controller_helper_funs.label_count(test_folder, 'test_', '_label', 20))