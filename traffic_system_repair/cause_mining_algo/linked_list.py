
"""
A linked list class where each link is given a fixed length when initialized.
If one sets the next values of nodes by hand, s/he can get a linked list with more than the fixedlength. If one uses the
built in functions such as AtBeginning, AtEnd, they always fix the length by dropping the tail whenever a node is added
until the current length of the list is equal to the set fixedlength.
Note that a node with nextval == None must be added to the list with the built in functions for the fixedlength to be
protected. The first lint of DropTail() function can bu uncommented if one cannot guarantee the adding of solely
Node's with nextval = None, i.e. Nodes with len == 1.
"""
class Node:
    def __init__(self, dataval=None):
        self.dataval = dataval
        self.nextval = None

class SLinkedList:
    def __init__(self, fixedlength):
        self.head = None
        self.fixedlength = fixedlength  # given fixed length of the list
        self.len = 0  # current length of the list

    def DropTail(self):

        #self.SetLenofList()  # too safe, too inefficient
        if not self.len == 0:
            self.len -= 1

        if self.head == None:
            print "tail does not exist"

        elif self.head.nextval == None:
            HeadVal = self.head.nextval
            self.head = None
            HeadVal = None
        else:
            node = self.head
            while(node.nextval.nextval):
                node = node.nextval

            node.nextval = None

    def listprint(self):
        printval = self.head
        while printval is not None:
            print (printval.dataval)
            printval = printval.nextval

    def AtBegining(self, newdata):
        NewNode = Node(newdata)

        # Update the new nodes next val to existing node
        NewNode.nextval = self.head
        self.head = NewNode
        self.len += 1

        while self.len > self.fixedlength:
            self.DropTail()

# Function to add newnode
    def AtEnd(self, newdata):

        self.len += 1
        NewNode = Node(newdata)
        if self.head is None:
            self.head = NewNode
        else:
            laste = self.head
            while(laste.nextval):
                laste = laste.nextval
            laste.nextval=NewNode

        while self.len > self.fixedlength:
            self.DropTail()

# Function to add node
    def Inbetween(self,middle_node,newdata):
        #  adds a new node "next" to the given node
        if middle_node is None:
            print("The mentioned node is absent")
            return

        NewNode = Node(newdata)
        NewNode.nextval = middle_node.nextval
        middle_node.nextval = NewNode
        self.len += 1

        while self.len > self.fixedlength:
            self.DropTail()

# Function to remove node
    def RemoveNode(self, Removekey):

        if not self.len == 0:
            self.len -= 1

        HeadVal = self.head

        if (HeadVal is not None):
            if (HeadVal.dataval == Removekey):
                self.head = HeadVal.nextval
                HeadVal = None
                return

        while (HeadVal is not None):
            if HeadVal.dataval == Removekey:
                break
            prev = HeadVal
            HeadVal = HeadVal.nextval

        if (HeadVal == None):
            return

        prev.nextval = HeadVal.nextval

        HeadVal = None

    def CheckLenofList(self):

        if self.head == None:
            return 0

        node = self.head
        len=1
        while node.nextval is not None:
            len+=1
            node = node.nextval
        return len

    def SetLenofList(self):
        self.len = self.CheckLenofList()

    def CheckIfAllIs(self, value):

        if not self.len == self.fixedlength:
            return False

        node = self.head
        while node is not None:
            if not node.dataval == value:
                return False
            node = node.nextval
        return True

def linked_list_main():
    list1 = SLinkedList(fixedlength=3)
    list1.head = Node("Mon")
    e2 = Node("Tue")
    e3 = Node("Wed")
    # Link first Node to second node
    list1.head.nextval = e2

    # Link second Node to third node
    e2.nextval = e3

    list1.AtBegining("Sun")
    list1.AtEnd("Thu")
    list1.Inbetween(list1.head.nextval, "Fri")
    list1.RemoveNode("Fri")
    print list1.CheckLenofList()

    list1.listprint()

    list = SLinkedList(fixedlength=0)
    list.AtBegining(1)
    list.AtBegining(2)
    list.AtBegining(3)

    list.listprint()

    list.AtBegining(4)

    list.listprint()

    list.AtBegining(5)
    list.AtBegining(6)

    list.listprint()

    print list.CheckIfAllIs(1)

    list.AtBegining(1)
    list.AtBegining(1)
    list.AtBegining(1)

    print list.CheckIfAllIs(1)


