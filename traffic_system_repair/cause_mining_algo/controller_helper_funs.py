import re
from trace_checker import STL
import copy

"""
helper functions for controller construction
"""

def break_formula(cause_formula):
    """
    Breaks a cause formula Phi = Phi_1 | Phi_2 | ... | Phi_n into an array formula_list = [Phi_1, Phi_2, ..., Phi_n]
    where Phi_i = A[0,a](u_i = c_i) & P[1,1] (phi_i)
    Args:
        cause_formula:
    Returns: stn_list of all formula components of the given concatenated formula

    """
    stn = STL.SyntaxTreeNode()
    stn.initialize_node(cause_formula.split(), 0)
    return break_formula_helper(stn)


def break_formula_helper(stn):
    """
    Args:
        stn: (of cause formula)
    Returns: stn_list of all formula components of the given concatenated formula

    """
    broken_formula_list = []
    if not (stn.op == '|'):
        return [stn]
    broken_formula_list += break_formula_helper(stn.left_node)
    broken_formula_list += break_formula_helper(stn.right_node)
    return broken_formula_list


def give_formula_components(formulalist):
    """
    Takes a formula list of the form formula_list = [Phi_1, Phi_2, ..., Phi_n] for a cause formula Phi = Phi_1 | Phi_2 | ... | Phi_n
    where Phi_i = A[1,a](u_i = c_i) & P[1,1] (phi_i).
    (which is the output of break_formula(cause_formula)
    returns a 2 dimensional array formula_components:
    A[0,a_0-2] (u_0 = c_0)   phi_0
    A[0,a_1-2] (u_1 = c_1)   phi_1
    ...
    A[0,a_n-2] (u_n = c_n)   phi_n

    Args:
        formulalist: stn list of cause formulas

    Returns: formula_components (2 dimensional array of stns)


    """
    formula_list = copy.deepcopy(formulalist)
    formula_components = [[None for x in xrange(0, 2)] for y in range(len(formula_list))]
    for i in xrange(0, len(formula_list)):
        if formula_list[i].op == '&':
            formula_components[i][0] = formula_list[i].left_node
        else:
            formula_components[i][0] = formula_list[i]
        if formula_components[i][0].interval.lb == 1:  # just double checking
            formula_components[i][0].interval.lb = 0
        else:
            print "least bound of A on lefthand side should've been 1, but it is not. WHY?!?!?"
        formula_components[i][0].interval.ub -= 2
        if formula_list[i].op == '&':
            formula_components[i][1] = formula_list[i].right_node.left_node

    return formula_components


def give_formula_components_old(formulalist):
    """
    Takes a formula list of the form formula_list = [Phi_1, Phi_2, ..., Phi_n] for a cause formula Phi = Phi_1 | Phi_2 | ... | Phi_n
    where Phi_i = A[0,a](u_i = c_i) & P[1,1] (phi_i).
    (which is the output of break_formula(cause_formula)
    returns a 2 dimensional array formula_components:
    A[0,a_0-1] (u_0 = c_0)   phi_0
    A[0,a_1-1] (u_1 = c_1)   phi_1
    ...
    A[0,a_n-1] (u_n = c_n)   phi_n

    * Note that we changed A[0,a] to A[1,a] for we need the later for calculation.

    Args:
        formulalist: stn list of cause formulas

    Returns: formula_components (2 dimensional array of stns)


    """
    formula_list = copy.deepcopy(formulalist)
    formula_components = [[None for x in xrange(0, 2)] for y in range(len(formula_list))]
    for i in xrange(0, len(formula_list)):
        if formula_list[i].op == '&':
            formula_components[i][0] = formula_list[i].left_node
        else:
            formula_components[i][0] = formula_list[i]
        formula_components[i][0].interval.ub -= 1
        if formula_list[i].op == '&':
            formula_components[i][1] = formula_list[i].right_node.left_node

    return formula_components

def label_count(folder_name, label_file_base, label_file_rest, trace_count):
    """
    count 1 labeled data points in the files in a folder
    Returns: violation_count (integer)

    """
    i = 0
    viol_cnt = 0
    for i in xrange(0, trace_count):
        label_file_name = folder_name + label_file_base + str(i) + label_file_rest
        with open(label_file_name, 'r') as lf:
            contents = lf.read()
            contents = re.split('\n| ', contents)
            viol_cnt += -1 + (contents.count('1'))
    return viol_cnt

def safe_remove(list, value):

    list_removed = copy.deepcopy(list)
    try:
        list_removed.remove(value)
    except ValueError:
        pass
    return list_removed