"""
Signal generator.
"""
from trace_checker import STL
from cause_mining_algo import controller_helper_funs as hf
import sys
sys.path.insert(0, '../traffic-network-code/traffic_network/')
sys.path.insert(0, '../traffic-network-code')
import network
import read_input
import copy
import random
from itertools import product
from cause_mining_algo import linked_list as ll

def xk_tolist(xk):

  if not type(xk) == list:
    try:
      xk = xk.tolist()
    except:
      try:
        xk = list(xk)
      except:
        print "xk is not a list, numpy array or a tuple. We must convert it to a list somehow for the controller."
  return xk


def controller(folder_name, name, trace_count,  duration, viol_formula, cause_formula, step_function, xk_initialized,
               uk_domain, uk_count, num_to_let=False):

  """
  IMPORTANT!:  We make the assumption that an input value (control or system) is ordered in the following way:
  The first xk_count numbers starting from 0 are for system inputs, the following uk_count ones, starting from xk_count
  are control inputs. For example, if xk_count = 3 and uk_count = 2, x0,x1 and x2 are system inputs and x3,x4 are control
  inputs. We also make the assumption that uk_domain is the same for each control input.
  Args:
    folder_name:
    name:
    trace_count:
    viol_formula:
    duration:
    cause_formula: (prefix) This is None if we are not constructing a controller, has a value otherwise.
    step_function: a function xk, _ = step(xk,uk) that takes old xk and uk values and returns the new xk value with
    another unimportant value.
    uk_domain: (list) of domain of uk values
    uk_count: the number of control inputs
    xk_initialized: (tuple, numpy array or list) initialized xk values
    num_to_let:(bool) if step_function takes xk's in the form of letters a and b instead of numbers 1 and 0, this value
    must be given as True. This variable is set specifically for traffic network's step function's needs.

  """
  npml = 0  # "no possible modes left" count
  #viol_cnt = 0
  xk_count = len(xk_initialized)
  iter_uk_crossproduct = product(uk_domain, repeat=uk_count)
  uk_crossproduct = []
  # set list of all possible uk combinations
  for prod in iter_uk_crossproduct:
      uk_crossproduct.append(prod)

  # First construct the traffic network.
  # In loop, generate trace. check it against the formula, produce the label. Save both

  tc = 0
  while tc < trace_count:
    f_signal_name = folder_name + "/" + name + "_" + str(tc)
    f_label_name = f_signal_name + "_label"
    f_s = open(f_signal_name, 'w')
    f_l = open(f_label_name, 'w')

    time = 0
    # initialize the states of control and system inputs
    xk = xk_initialized
    xk = xk_tolist(xk)

    # Initialize uk values randomly
    #rand_index = random.randint(0, len(uk_crossproduct))
    #uk = list(uk_crossproduct[rand_index])

    # break the formula into its components
    formula_list = hf.break_formula(cause_formula)
    formula_components = hf.give_formula_components(formula_list)

    # Initialize the checker:
    stn_viol = STL.SyntaxTreeNode()
    stn_viol.initialize_node(viol_formula.split(), 0)

    # Initialize the linked_lists array which will store the necessary data to compute formula[i][0]s
    linked_lists = []
    for i in xrange(len(formula_list)):
        length = formula_components[i][0].interval.ub + 1  # Note that if original formula is A 1 1, then fixedlength==0, thus list.CheckIfAllIs(n) returns True for all n
        linked_lists.append(ll.SLinkedList(fixedlength=length))

    while time <= duration:

        # new uk's free values are determined upon old xk and uk values
        uk_works = False
        free_control_values = copy.deepcopy(uk_crossproduct)
        while not uk_works:
            # set uk randomly from free_control_values
            rand_index = random.randint(0, len(free_control_values)-1)  # very weirdly random.randint(a,b) can give out b
            uk = list(free_control_values[rand_index])

            j = 0
            while j in xrange(len(formula_list)):
                # check if left side is satisfied by checking if A 1, b-1 (u=c) is satisfied - with the linked list,
                # that's the data it carries- and if u = c at this randomly generated uk.
                control_input = formula_components[j][0].left_node.metric - xk_count
                control_input_val = formula_components[j][0].left_node.param
                if linked_lists[j].CheckIfAllIs(control_input_val) and uk[control_input] == control_input_val:
                    # left side is satisfied for A 0 b-2 ( u = c ), check if right formula is satisfied
                    right_side_satisfied = False
                    if formula_components[j][1] == None:
                        right_side_satisfied = True
                    else:
                        copy_stn = copy.deepcopy(formula_components[j][1])  # deepcopy may not work here
                        copy_stn.compute_qv(STL.DataPoint(value=xk + uk, time=time))
                        if copy_stn.qv > 0:
                            right_side_satisfied = True
                    if right_side_satisfied:
                        break
                j += 1

            if j == len(formula_list):
                uk_works = True
            else:
                free_control_values = hf.safe_remove(free_control_values, tuple(uk))
            if len(free_control_values) == 0:
                print "No possible free control combinations left"
                npml += 1
                rand_index = random.randint(0, len(uk_crossproduct)-1)
                uk = list(uk_crossproduct[rand_index])
                uk_works = True

        # we found a working uk for the corresponding xk
        # update all stns and linked_list values with this uk and xk values
        for i in xrange(len(formula_list)):
            if formula_components[i][1] is not None:
                formula_components[i][1].compute_qv(STL.DataPoint(value=xk + uk, time=time))
            control_input = formula_components[i][0].left_node.metric - xk_count
            linked_lists[i].AtBegining(uk[control_input])

        # compute label, write new xk, uk and label to the file
        stn_viol.compute_qv(STL.DataPoint(value=xk + uk, time=time))
        f_s.write("%s %s\n" % (str(time), " ".join([str(x) for x in xk + uk])))
        qual = 0 if stn_viol.qv >= 0 else 1
        f_l.write("%s %s\n" % (str(time), str(qual)))

        # new xk is calculated from this loop's xk and uk values
        if num_to_let:
            uk_letters = ['a' if x == 0 else 'b' for x in uk]
            xk, _ = step_function(xk, uk_letters)
        else:
            xk, _ = step_function(xk, uk)

        xk = xk_tolist(xk)

        time += 1


    f_s.close()
    f_l.close()
    tc += 1

  print "There were no possible modes left for a controller input for " + str(npml) + " times."