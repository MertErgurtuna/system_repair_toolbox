import sys
from collections import namedtuple

# MIN-MAX CONSTANTS
MAX_EVAL = sys.maxsize
MIN_EVAL = -sys.maxsize

#CATEGORY CONSTANTS
CATEGORY_MAXIMIZATION = 0
CATEGORY_MINIMIZATION = 1
#MONOTONIC CONSTANTS
MONOTONICALLY_INCREASING =0
MONOTONICALLY_DECREASING =1
MONOTONICITY_NA=2


FormulaValuation = namedtuple('FormulaValuation', ['formula', 'valuation'])


# Aligned with names.

CATEGORIES = [CATEGORY_MINIMIZATION, CATEGORY_MINIMIZATION, CATEGORY_MAXIMIZATION,
              CATEGORY_MAXIMIZATION, CATEGORY_MAXIMIZATION, CATEGORY_MAXIMIZATION,
              CATEGORY_MINIMIZATION, CATEGORY_MAXIMIZATION, CATEGORY_MAXIMIZATION]

OBJECTIVE_NAMES = ['mismatch', 'ratio', 'detailed', 'precision', 'recall', 'f_score', 'qv', 'tp', 'tn']


class OptimizationObjective:

    def __init__(self, name='mismatch', beta=0.15, bound=-1, criteria=None):
        # The id corresponds to 'mismatch', 'ratio', 'f_score', 'precision', 'recall'
        self.name = name # 0 for mismatch
        self.category = CATEGORIES[OBJECTIVE_NAMES.index(self.name)]
        self.beta = beta  # only used for f_score.
        self.bound = bound # used in bounding the counter criteria, such as maximize TP bound FP
        # Negative bound means it is not set.
        self.criteria = criteria
        if bound >= 0 and not criteria: # If the bound is set but not the criteria, define it as the complementary criteria.
            if self.name == 'tp':
                self.criteria = 'fp'
            if self.name == 'tn':
                self.criteria = 'fn'

    def check_bound(self, result):
        """result is an evaluation result, check if it satisfies the bound."""
        if self.bound < 0:
            return True # No bound is set, return True
        if self.criteria == 'fp':
            return True if result.fp <= self.bound else False
        if self.criteria == 'fn':
            return True if result.fn <= self.bound else False




# TODO: we need to fix that structure. There should be a class, and the min/max constraints should be embedded to that
# class. The current structure is bad 