from constants import stl_constants
from optimization import diagonal_search
from optimization import evaluation_result
from optimization import evaluator
from optimization import formula_search
from optimization import grid_search
from signal_generation import generator
from trace_checker import formula_generator
from trace_checker import STL


def check_formula_for_oscillation():
    folder_name = 'test_data/oscillation/'
    signal_file_base = 'sgt'
    trace_count = 200
    pc = 48  #
    parameter_domains = {'p0': range(0, 300, 1), 'p1': range(600, 1200, 1), 'p2': range(0, 2, 1), 'p3': range(1, 10, 1)}
    template_formulas = [
        ' P 1 1 ( ( x0 > p1 ) ) & P 1 1 ( x2 = 0 ) ',
        ' P 1 1 ( ( x1 > p1 ) ) & P 1 1 ( x2 = 0 ) ',
        ' P 1 1 ( ( x0 < p0 ) ) & P 1 1 ( x2 = 1 ) ',
        ' P 1 1 ( ( x1 < p0 ) ) & P 1 1 ( x2 = 1 ) '
    ]
    bound = 0
    max_d = 6
    v = diagonal_search.search_template_formulas(template_formulas, folder_name, signal_file_base, trace_count, pc,
                                                 parameter_domains, bound, max_d,
                                                 log_file_name="log", verbose=True)


def synthesize_repairable_formulas_for_ta():

  folder_name = 'test_data/ta_repair/'
  signal_file_base = 'sgt'
  trace_count = 10
  pc = 8 # processor count

  location_list = ['0'] # maps to metric list. A metric for each TA in NTA
  location_counts = [4]
  set_valued_metrics = location_list # each of them is set valued.

  # For each TA, parameter domain is the possible location ids.
  # pAb : parameter for guard less than repair
  # pPb : parameter for guard greater than repair
  # pAIb : parameter for invariant repair

  parameter_domains = {'p0' : range(0, location_counts[0]),
                       'pAb' : range(0, 5),
                       'pPb' : range(0, 5),
                       'pAIb' : range(0, 5)}


  glr_formulas, ggr_formulas, inv_formulas = formula_generator.generate_repairable_formulas_for_ta(
    location_list, [i - 2 for i in location_counts])

  print(glr_formulas)
  print("******************")
  print(ggr_formulas)
  print("******************")
  print(inv_formulas)

  # Fix parameters (ta, location count etc)
  # run diagonal search for glr_formulas + ggr_formulas + inv_formulas

  bound = 300
  max_d = 2
  diagonal_search.search_template_formulas(glr_formulas +  ggr_formulas, folder_name, signal_file_base, trace_count, pc, parameter_domains,  bound, max_d, log_file_name="log", verbose=True)


def check_formula_for_ta():

    formula = "& x1 = 1 x3 > 9"
    folder_name = 'test_data/train_gate/traces/'

    signal_file_base = 'trace'
    trace_count = 100
    result = evaluator.evaluate_signals(formula, folder_name, signal_file_base, trace_count, "", skip_count=5)
    print(formula)
    print(result)

def search_all_formulas_ta():
    """ Prepares parameters, calls formula search method for ta data (train gate example)."""

    folder_name = 'test_data/train_gate/traces_no_train/'
    # folder_name = '/Users/ebru/Dropbox/ONGOING_PROJECTS/MSCA/traces/traces/'
    signal_file_base = 'trace'
    trace_count = 5
    pc = 6  #
    operator_counts = [0,1,2,3,4,5]

    # Train: {Far: 0, Near: 1, In: 2} | Controller: {C0: 0, C1: 1, C2: 2, C3: 3} | Gate: {Up: 0, ComingDown: 1, Down: 2, GoingUp: 3} | z
    parameter_domains = {'p0': range(0, 3, 1), 'p1': range(0,4,1),
			 'pA': range(0, 60, 5), 'pP': range(0, 60, 5), 'pS': range(0, 2, 2)}

    result_file = 'min_max'
    metric_list = ['0', '1']
    set_valued_metrics = ['0','1']

    # MISMATCH
    return_type = stl_constants.OptimizationObjective(name='mismatch')
    best_formulas, formula, results = formula_search.formula_search(
        metric_list=metric_list,
        set_valued_metrics=set_valued_metrics,
        operator_counts=operator_counts,
        parameter_domains=parameter_domains, folder_name=folder_name,
        trace_count=trace_count,
        signal_file_base=signal_file_base, process_count=pc,
        save=True,
        return_type=return_type, result_file=result_file, withoutS=True,
        log_file_name="log_25_4_2", skip_count=5)
    #return_type = stl_constants.OptimizationObjective(name='tp', bound=1)
    #max_d = 2
    #v = diagonal_search.search_all_formulas(folder_name, signal_file_base, trace_count, pc,
    #                                        operator_counts, parameter_domains, metric_list,
    #                                        set_valued_metrics, return_type, max_d,
    #                                        log_file_name="log", verbose=True)

    print(best_formulas)
    formula = best_formulas[len(best_formulas)-1].formula
    #formula_prefix = STL.infix_to_prefix(formula.formula)
    print(formula)
    return best_formulas
    #return evaluate_signals(formula_prefix, folder_name, signal_file_base, 3)



def traffic_signal_generator():
    # This function uses the package "signal_generation". This is for "cause mining."
    # generates new traffic_signal data with given violation_formula, trace_count and duration.
    folder_name = "test_data/traffic_data/test2/"
    viol_formula_yes_soln = "x0 < 30 & x1 < 30 & x2 < 30 & x3 < 15 & x4 < 15"  # viol formula for l5_system
    viol_formula_no_soln = "x0 < 32 & x1 < 32 & x2 < 32 & x3 < 16 & x4 < 16"  # viol formula for l5_system_no_soln
    viol_formula_prefix = STL.infix_to_prefix(viol_formula_yes_soln)
    trace_count = 20
    duration = 100
    generator.generate_traffic_traces(folder_name=folder_name, file_name="test", trace_count=trace_count,
                                      viol_formula=viol_formula_prefix, traffic_file="test_data/traffic_data/l5_system",
                                      duration=duration, pc=4)



""" Formula Generation Simplification . """
def formula_generator_simplification():

  print("Generate parametric formulas with and without simplification, compare results.")
  metric_list = ['0', '1']
  set_valued_metrics = []
  operator_counts = [0, 1, 2]
  print_details = False
  for operator_count in operator_counts:
    formula_list = formula_generator.generate_formula_tree_iterative(metric_list, operator_count,
                                                                     return_formula_string=True,
                                                                     set_valued_metrics=set_valued_metrics,
                                                                     simplify=False);
    formula_list_simplified = formula_generator.generate_formula_tree_iterative(metric_list, operator_count,
                                                                     return_formula_string=True,
                                                                     set_valued_metrics=set_valued_metrics,
                                                                     simplify=True);
    print("\t\t\tRESULTS FOR OPERATOR COUNT %d" % operator_count)
    print("\t\t Formula count %d" % len(formula_list))
    print("\t\t Formula count - simplified %d" % len(formula_list_simplified))
    print("\t\t Difference %d" % (len(formula_list) - len(formula_list_simplified)))
    if print_details:
      removed_formula = set(formula_list)
      removed_formula.difference_update(set(formula_list_simplified))
      i=1
      for f in removed_formula:
        print("\t\t\t\t %i   ----   %s" % (i, f))
        i += 1

      print(" All formulas in the original list:")
      i=1
      for f in formula_list:
        print("\t\t\t\t %i   ----   %s" % (i, f))
        i += 1

      print(" All formulas in the simplified list:")
      i = 1
      for f in formula_list_simplified:
        print("\t\t\t\t %i   ----   %s" % (i, f))
        i += 1
