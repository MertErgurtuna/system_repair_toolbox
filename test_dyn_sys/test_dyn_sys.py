import subprocess as sp
import sys

def test_traffic_sys_only_x1():
    output_test = sp.check_output("""cd ../; python3 synthesize_for_dyn_sys.py 1; cd -""", shell=True).split("\n")
    formulas = output_test[0].split(" TP: ")[0].split("|")
    print (formulas)


def test_traffic_sys_all_roads():
    output_test = sp.check_output("""cd ../; python3 synthesize_for_dyn_sys.py 0; cd -""", shell=True).split("\n")
    formulas = output_test[0].split(" TP: ")[0].split("|")
    print (formulas)

def synthesize_controller_x1():
    output_test = sp.check_output("""cd ../traffic_system_repair; python2 main.py 1; cd -""", shell=True).split("\n")
    print (output_test)

def synthesize_controller_all():
    output_test = sp.check_output("""cd ../traffic_system_repair; python2 main.py 0; cd -""", shell=True).split("\n")
    print (output_test)

if __name__ == '__main__':
    if (sys.argv[1] == 'only_x1'):
        test_traffic_sys_only_x1()
        synthesize_controller_x1()
    elif(sys.argv[1] == 'all_roads'):
        test_traffic_sys_all_roads()
        synthesize_controller_all()

