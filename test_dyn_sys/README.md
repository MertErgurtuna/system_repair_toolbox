Since traffic system repairer is implemented in Python2 and our other scripts are implemented in Python3, experiment setup needs both Python2 and Python3. Provided this executable is for 64-bit Linux machines, for other environments replace this executable as needed. Run experiments from the `stl_fs_sm/test_dyn_sys/` directory.

To run experiments for the traffic system example for only congestion on link 1:
`python2 test_dyn_sys.py only_x1`

To run experiments for the traffic system example for congestion on all roads:
`python2 test_dyn_sys.py all_roads`
